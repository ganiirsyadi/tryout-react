import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import './App.css';
import { Header } from './components/Header';
import { TryoutSoal } from './containers/TryoutSoal';
import { Mulai } from './containers/Mulai';
import { TryoutProvider } from './contexts/TryoutState'
import { Hasil } from './containers/Hasil';

function App() {
  return (
    <TryoutProvider>
      <Router>
        <Header />
        <Switch>
          <Route component={TryoutSoal} path="/tryout" />
          <Route component={Hasil} path="/hasil" />
          <Route component={Mulai} exact path="/" />
        </Switch>
      </Router>
    </TryoutProvider>
  );
}

export default App;
