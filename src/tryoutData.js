export const data = {
   "error": false,
   "paket": {
     "id": 2,
     "nama": "Try Out Free 1",
     "jenis_tryout": 0,
     "password": null,
     "soal_tkp": [
       {
         "id": 36,
         "soal": "Anda baru saja dilantik menjadi pejabat pengadaan barang pada salah satu Kementerian. Ketika Anda berkoordinasi dengan pejabat pengadaan barang lainnya ternyata mereka sudah terbiasa untuk menerima tanda terima kasih atas proses pengadaan yang telah dilakukan. Namun Anda mengetahui bahwa hal dimaksud adalah salah satu gratifikasi, sehingga hal itu tidaklah diperbolehkan. Terkait dengan hal dimaksud maka sikap Anda ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Tidak ingin melakukannya sama sekali.",
             2
           ],
           [
             "Menegur pejabat pengadaan yang menerima gratifikasi tersebut secara langsung, supaya dapat diproses secara kekeluargaan terlebih dahulu.",
             3
           ],
           [
             "Melaporkan pada atasan langsung pejabat yang menerima gratifikasi, sehingga dapat diproses secara internal dan tidak mencoreng nama instansi.",
             4
           ],
           [
             "Berusaha semampunya untuk tidak melakukannya.",
             1
           ],
           [
             "Melaporkan pada unit kepatuhan untuk dilakukan penindakan sesuai aturan hukuman disiplin pegawai yang berlaku.",
             5
           ]
         ],
         "jawaban_skor": [
           "Melaporkan pada unit kepatuhan untuk dilakukan penindakan sesuai aturan hukuman disiplin pegawai yang berlaku.",
           5
         ]
       },
       {
         "id": 37,
         "soal": "Anda bekerja pada instansi yang memiliki anggaran belanja cukup besar, karena lemahnya pengendalian internal pada instansi Anda banyak terjadi penyelewengan keuangan, yang salah satunya dilakukan oleh atasan Anda yang melakukan rekayasa laporan keuangan kantor, maka anda ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Hal semacam itu memang sudah menjadi tradisi yang tidak baik di Indonesia.",
             1
           ],
           [
             "Mengingatkan dan melaporkan kepada yang berwenang.",
             5
           ],
           [
             "Hal tersebut sering terjadi di kantor manapun.",
             2
           ],
           [
             "Anda sangat tidak menyetujui hal tersebut.",
             4
           ],
           [
             "Tidak ingin terlibat dalam proses rekayasa tersebut.",
             3
           ]
         ],
         "jawaban_skor": [
           "Mengingatkan dan melaporkan kepada yang berwenang.",
           5
         ]
       },
       {
         "id": 38,
         "soal": "Setiap Pejabat Eselon 3 pada instansi Anda memperoleh kendaraan dinas. Dikarenakan ada kerjaan kantor pada akhir pekan Anda menggunakan kendaraan dinas Eselon III Anda, secara tidak sengaja Anda menabrakkan kendaraan tersebut. Maka tindakan yang seharusnya Anda lakukan pertama kali adalah ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Membawa kendaraan tersebut ke bengkel, melaporkan kejadian tersebut kepada pimpinan serta menyerahkan segala keputusan kepada pimpinan.",
             5
           ],
           [
             "Mencoba memperbaiki sendiri kendaraan tersebut.",
             3
           ],
           [
             "Menanyakan pada Eselon III Anda apakah mobil tersebut diasuransikan, sehingga Anda dapat mengurus klaim asuransi dimaksud.",
             2
           ],
           [
             "Membawa kendaraan tersebut ke bengkel atas biaya pribadi dan mengembalikan dalam keadaan utuh kembali.",
             4
           ],
           [
             "Melaporkan kejadian tersebut kepada pimpinan dan siap menerima hukuman/petunjuk dari pimpinan.",
             1
           ]
         ],
         "jawaban_skor": [
           "Membawa kendaraan tersebut ke bengkel, melaporkan kejadian tersebut kepada pimpinan serta menyerahkan segala keputusan kepada pimpinan.",
           5
         ]
       },
       {
         "id": 39,
         "soal": "Anda adalah auditor yang dipercayakan untuk menyelesaikan laporan hasil pemeriksaan yang bersifat rahasia ketika belum dipublikasikan. KEtika Anda berkumpul dengan teman-teman kantor bnyak yang membicarakan objek pemeriksaan Anda, dan Anda mengetahui bahwa hal yang dibicarakan oleh rekan-rekan Anda tersebut tidak sesuai dengan fakta yang ada. Terkait dengan hal tersebut Anda akan ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Akan mengalihkan ke pembicaraan lain apabila mereka sudah mulai menyinggung tugas baru saya.",
             2
           ],
           [
             "Membicarakan hal-hal lain yang tidak ada kaitannya dengan tugas saya.",
             3
           ],
           [
             "Menegur teman saya dan menyampaikan bahwa hal tersebut tidak boleh dilakukan.",
             5
           ],
           [
             "Akan meluruskan kepada teman-teman Anda bahwa isu dimaksud tidaklah benar.",
             1
           ],
           [
             "Membatasi pembicaraan agar tidak menyangkut ke hal-hal tugas saya.",
             4
           ]
         ],
         "jawaban_skor": [
           "Menegur teman saya dan menyampaikan bahwa hal tersebut tidak boleh dilakukan.",
           5
         ]
       },
       {
         "id": 40,
         "soal": "Apabila penilaian diri saya jelek, maka saya akan bertindak ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Meminta masukan dari rekan kerja saya.",
             2
           ],
           [
             "Belajar lebih giat lagi.",
             3
           ],
           [
             "Mengevaluasi diri saya dan melakukan perbaikan.",
             5
           ],
           [
             "Memperbaiki diri saya.",
             4
           ],
           [
             "Mawas diri.",
             1
           ]
         ],
         "jawaban_skor": [
           "Mengevaluasi diri saya dan melakukan perbaikan.",
           5
         ]
       },
       {
         "id": 41,
         "soal": "Ketika berhasil dalam menyelesaikan tugas, saya akan ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Tidak perlu lagi berusaha.",
             1
           ],
           [
             "Tetap berusaha sekuat tenaga.",
             3
           ],
           [
             "Untuk tugas selanjutnya, akan mengerjakan dengan lebih baik lagi.",
             4
           ],
           [
             "Tidak mudah puas dan berusaha lebih baik lagi.",
             5
           ],
           [
             "Berusaha sekedarnya.",
             2
           ]
         ],
         "jawaban_skor": [
           "Tidak mudah puas dan berusaha lebih baik lagi.",
           5
         ]
       },
       {
         "id": 42,
         "soal": "Anda adalah petugas pelayanan yang berhubungan langsung dengan masyarakat. Pada suatu hari Anda mendapat penugasan untuk menjadi Ketua Tim dalam kompetisi pelayanan prima yang akan diikuti oleh kantor Anda. Namun di sisi lain Anda menyadari bahwa pada bulan ini adalah periode puncak dimana pengunjung sangat banyak berdatangan ke kantor Anda. Menyikapi hal dimaksud maka sikap Anda adalah...",
         "pembahasan": null,
         "pilihan": [
           [
             "Menyusun strategi melalui pemetaan kompetitor yang mengikuti kompetisi dimaksud, agar kantor Anda terlihat paling siap mengikuti kompetisi dimaksud.",
             2
           ],
           [
             "Menyampaikan kepada atasan Anda bahwa pengunjung adalah prioritas, sehingga sangat tidak dimungkinkan dalam periode dimaksud untuk mengikuti kompetisi.",
             1
           ],
           [
             "Menyusun strategi untuk mengalokasikan pegawai agar dapat fokus kepada fungsi pelayanan dan kompetisi dimaksud, karena Anda meyakini kedua hal tersebut dapat dituntaskan secara bersamaan.",
             5
           ],
           [
             "Mengingat ketua tim adalah amanah bagi Anda, maka saya akan berfokus pada kompetisi diaksud untuk dapat memenangkan juara satu.",
             3
           ],
           [
             "Segera membentuk tim dan merekrut anggota untuk selanjutnya dapat dikoordinasikan pembagian penugasan untuk mempersiapkan mengikuti kompetisi dimaksud.",
             4
           ]
         ],
         "jawaban_skor": [
           "Menyusun strategi untuk mengalokasikan pegawai agar dapat fokus kepada fungsi pelayanan dan kompetisi dimaksud, karena Anda meyakini kedua hal tersebut dapat dituntaskan secara bersamaan.",
           5
         ]
       },
       {
         "id": 43,
         "soal": "Saya ditugaskan di front office untuk melayani tamu pimpinan. Pada saat pimpinan saya tidak berada di tempat dan ada tamu pimpinan yang memerlukan keputusan segera, sikap saya adalah ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Mengambil keputusan tanpa petunjuk atasan selama tidak bertentangan dengan kebijakan umum pimpinan",
             4
           ],
           [
             "Menghubungi atasan saya dan meminta arahan terkait permasalahan dimaksud, untuk selanjutnya disampaikan pada tamu pimpinan.",
             5
           ],
           [
             "Mengambil keputusan tanpa petunjuk atasan karena sangat mendesak.",
             1
           ],
           [
             "Tidak berani mengambil keputusan tanpa petunjuk atasan saya, karena hal dimaksud di luar wewenang saya.",
             2
           ],
           [
             "Mempersilahkan tamu pimpinan Anda untuk menunggu, dan menyampaikan bahwa keputusan dapat diambil setelah pimpinan anda datang.",
             3
           ]
         ],
         "jawaban_skor": [
           "Menghubungi atasan saya dan meminta arahan terkait permasalahan dimaksud, untuk selanjutnya disampaikan pada tamu pimpinan.",
           5
         ]
       },
       {
         "id": 44,
         "soal": "Anda adalah pegawai pada Pemprov DKI Jakarta yang saat ini berencana melakukan perubahan sistem informasi, dari yang semula manual menjadi semua tersistem dengan Aplikasi. Anda mengetahui bahwa di unit kerja Anda banyak pegawai yang sudah berusia lanjut, sehingga apabila dilakukan sistem pada unit kerja Anda akan banyak pegawai yang mengalami kesulitan, maka sikap Anda adalah...",
         "pembahasan": null,
         "pilihan": [
           [
             "Membantu pendampingan pada pegawai yang kesulitan dalam menerapkan peralihan sistem, sehingga peralihan sistem dapat dilakukan dengan baik.",
             5
           ],
           [
             "Menyampaikan kepada atasan Anda untuk menunda implementasi perubahan sistem, hingga semua pegawai pada unit kerja Anda siap.",
             2
           ],
           [
             "Membantu bagian IT untuk melakukan pemetaan pegawai yang kemungkinan akan menghadapi kesulitan terkait implementasi kebijakan dimaksud.",
             4
           ],
           [
             "Meminta bagian IT untuk menganalisis kembali kebijakan dimaksud, karena akan menyulitkan banyak pegawai.",
             1
           ],
           [
             "Meminta bagian IT untuk tetap mengimplementasikan perubahan sistem, karena Anda mengetahui perubahan memang harus dilakukan.",
             3
           ]
         ],
         "jawaban_skor": [
           "Membantu pendampingan pada pegawai yang kesulitan dalam menerapkan peralihan sistem, sehingga peralihan sistem dapat dilakukan dengan baik.",
           5
         ]
       },
       {
         "id": 45,
         "soal": "Anda adalah ASN pada unit kerja yng memiliki beban kerja yang sangat tinggi. Pada suatu hari Anda melakukan survey pada pegawai di unit kerja Anda, mengenai perspektif untuk dapat bekerja optimal tanpa merasakan adanya tekanan yang tinggi. Salah satu masukan yang diperoleh adalah untuk menciptakan ruangan kerja yang kondusif, dengan mengimplementasikan konsep green office. Anda mengetahui bahwa anggaran kantor saat ini sangat terbatas, dan ingin melakukan efisiensi anggaran, maka sikap Anda adalah...",
         "pembahasan": null,
         "pilihan": [
           [
             "Mengusulkan kepada atasan Anda untuk melakukan perubahan penataan kantor, sehingga pegawai dapat bekerja dengan senang dan menurunkan tekanan kerja pada unit kerja Anda.",
             2
           ],
           [
             "Menggerakkan pegawai di lingkungan Anda untuk melakukan perubahan penataan kantor mandiri dengan konsep low budget, sehingga tujuan efisiensi anggaran dan penataan ruang kerja yang kondusif dapat tercapai.",
             5
           ],
           [
             "Membahas alternatif lain yang dapat menurunkan tekanan kerja dengan para pegawai di unit kerja Saudara.",
             4
           ],
           [
             "Menyampaikan kepada pegawai bahwa perubahan penataan kantor adalah ide yang bagus, namun karena adanya keterbatasan anggaran maka akan ditunda sampai tahun berikutnya.",
             3
           ],
           [
             "Mengabaikan masukan dari para pegawai karena Anda mengetahui hal tersebut sangat tidak dimungkinkan.",
             1
           ]
         ],
         "jawaban_skor": [
           "Menggerakkan pegawai di lingkungan Anda untuk melakukan perubahan penataan kantor mandiri dengan konsep low budget, sehingga tujuan efisiensi anggaran dan penataan ruang kerja yang kondusif dapat tercapai.",
           5
         ]
       },
       {
         "id": 46,
         "soal": "Di lingkungan kerja yang baru ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Menyesuaikan diri dengan lingkungan kerja yang baru, dengan membuat diri merasa nyaman pada lingkungan kerja baru.",
             1
           ],
           [
             "Memahami pola kerja dengan mempelajari semua SOP yang ada pada lingkungan kerja yang baru.",
             2
           ],
           [
             "Saya akan memahami target yang dipercayakan kepada saya, dan langsung menyusun rencana kerja.",
             5
           ],
           [
             "Terlebih dahulu bersosialisasi dengan rekan kerja, dan meminta arahan-arahan untuk kedepannya.",
             3
           ],
           [
             "Terlebih dahulu menghadap atasan, dan menanyakan apabila ada arahan yang kurang dipahami.",
             4
           ]
         ],
         "jawaban_skor": [
           "Saya akan memahami target yang dipercayakan kepada saya, dan langsung menyusun rencana kerja.",
           5
         ]
       },
       {
         "id": 47,
         "soal": "Prediksi pengamat ekonomi bahwa bulan depan akan terjadi inflasi besar di Indonesia membuat saya ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Yang akan terjadi, biarlah terjadi",
             2
           ],
           [
             "Bersikap tenang, karena hal tersebut merupakan hal wajar yang terjadi.",
             3
           ],
           [
             "Bersikap hati-hati dan berhemat dalam membelanjakan barang.",
             4
           ],
           [
             "Prediksi yang menyusahkan seperti itu tak perlu saya percayai",
             1
           ],
           [
             "Memetakan rencana belanja, agar belanja yng dilakukan tidak melebihi pendapatan yang diterima.",
             5
           ]
         ],
         "jawaban_skor": [
           "Memetakan rencana belanja, agar belanja yng dilakukan tidak melebihi pendapatan yang diterima.",
           5
         ]
       },
       {
         "id": 48,
         "soal": "Saya diutus mengikuti diklat. Oleh panitia penyelenggara saya ditempatkan sekamar dengan orang yang tidak saya kenal yang berasal dari kota lain. Sikap saya adalah ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Mengajukan keberatan tetapi akhirnya menerima aturan panitia..",
             3
           ],
           [
             "Menjelaskan kepada panitia, bahwa teman sekamar juga merupakan teman sat kelompok dalam diklat, untuk dapat bekerja sama dengan baik diperlukan sepemahaman dengan teman sekamar.",
             2
           ],
           [
             "Menerima aturan panitia dan berusaha mengenal dan memahami teman sekamar.",
             5
           ],
           [
             "Mengajukan keberatan dan meminta ditempatkan dengan minimal orang yang dikenal.",
             1
           ],
           [
             "Menerima aturan panitia.",
             4
           ]
         ],
         "jawaban_skor": [
           "Menerima aturan panitia dan berusaha mengenal dan memahami teman sekamar.",
           5
         ]
       },
       {
         "id": 49,
         "soal": "Bila ada rekan kerja yang salah memanggil nama saya, maka saya ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Saya mengingatkan kekeliruannya dengan baik-baik.",
             5
           ],
           [
             "Saya tidak tersinggung, apabila rekan kerja tersebut lebih tua dari saya.",
             1
           ],
           [
             "Hal seperti itu tak menjadi masalah bagi saya.",
             4
           ],
           [
             "Saya mengingatkannya dengan tegas agar dia dia tidak melupakan nama saya.",
             3
           ],
           [
             "Saya tersingung, karena nama adalah kehornatan seseorang.",
             2
           ]
         ],
         "jawaban_skor": [
           "Saya mengingatkan kekeliruannya dengan baik-baik.",
           5
         ]
       },
       {
         "id": 50,
         "soal": "Jika hal-hal kecil merusak rencana besar saya, maka ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Saya marah kepada pihak lain yang juga ikut bertanggungjawab akan hal ini.",
             1
           ],
           [
             "Saya akan melakukan evaluasi menyeluruh.",
             5
           ],
           [
             "Saya sangat sedih dan marah kenapa hal kecil mampu merusak rencana besar tersebut",
             3
           ],
           [
             "Tentu saja saya marah.",
             2
           ],
           [
             "Saya butuh waktu menenangkan diri.",
             4
           ]
         ],
         "jawaban_skor": [
           "Saya akan melakukan evaluasi menyeluruh.",
           5
         ]
       },
       {
         "id": 51,
         "soal": "Ketika sedang melakukan presentasi, saya menerima pesan singkat (SMS) yang mengabarkan bahwa anak saya diopname di rumah sakit. Reaksi saya adalah ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Mencari tahu kondisi anak saya kemudian memutuskan apakah akan tetap presentasi atau ke rumah sakit.",
             5
           ],
           [
             "Menghentikan presentasi dan mencari tahu kondisi anak saya.",
             4
           ],
           [
             "Membalas SMS dan melanjutkan presentasi.",
             3
           ],
           [
             "Menghentikan presentasi dan langsung menuju rumah sakit.",
             1
           ],
           [
             "Tetap melanjutkan presentasi, tetapi setelah selesai langsung pergi ke rumah sakit.",
             2
           ]
         ],
         "jawaban_skor": [
           "Mencari tahu kondisi anak saya kemudian memutuskan apakah akan tetap presentasi atau ke rumah sakit.",
           5
         ]
       },
       {
         "id": 52,
         "soal": "Ketika saya diterima sebagai ASN dan saya tidak mempunyai uang maka saya akan ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Mencari pinjaman ke teman sekantor.",
             3
           ],
           [
             "Mencari pinjaman dari atasan.",
             2
           ],
           [
             "Bekerja sampingan untuk mendapatkan uang.",
             4
           ],
           [
             "Mengundurkan diri dari PNS.",
             1
           ],
           [
             "Mengontrol pengeluaran, disesuaikan dengan pendapatan saya.",
             5
           ]
         ],
         "jawaban_skor": [
           "Mengontrol pengeluaran, disesuaikan dengan pendapatan saya.",
           5
         ]
       },
       {
         "id": 53,
         "soal": "Atasan anda menetapkan target tugas harus selesai pada deadline tanggal 28 bulan ini, maka ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Saya akan selesaikan tepat pada tanggal 28.",
             4
           ],
           [
             "Saya menegosiasikan deadline yang ditetapkan atasan tersebut dengan baik-baik agar tidak terlalu memberatkan.",
             1
           ],
           [
             "Kalau tugas lain menumpuk, saya akan minta izin untuk menyelesaikan barang satu atau dua hari setelah deadline.",
             2
           ],
           [
             "Saya meminta tolong rekan lain agar tidak terlambat deadline.",
             3
           ],
           [
             "Saya mencoba menyelesaikan tanggal 27 jika memungkinkan.",
             5
           ]
         ],
         "jawaban_skor": [
           "Saya mencoba menyelesaikan tanggal 27 jika memungkinkan.",
           5
         ]
       },
       {
         "id": 54,
         "soal": "Draft laporan yang dibuat oleh tim kerja saya ditolak oleh atasan karena dianggap kurang visible. Sikap saya adalah ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Berusaha mencari penyebab mengapa laporan dimaksud dianggap kurang visible.",
             3
           ],
           [
             "Segera melakukan perbaikan atas draft laporan dan mangajukan kembali.",
             5
           ],
           [
             "Menerima penolakan dan berusaha memperbaiki.",
             4
           ],
           [
             "Menerima penolakan tersebut, karena ada pertimbangan kurang visible-nya proyek dimaksud.",
             1
           ],
           [
             "Berkoordinasi dengan rekan sejawat yang sama-sama mengerjakannya.",
             2
           ]
         ],
         "jawaban_skor": [
           "Segera melakukan perbaikan atas draft laporan dan mangajukan kembali.",
           5
         ]
       },
       {
         "id": 55,
         "soal": "Pada saat melaksanakan tugas berat dan menuntut kemampuan tinggi, saya akan...",
         "pembahasan": null,
         "pilihan": [
           [
             "Fokus untuk menyelesaiakan tugas dimaksud dengan segenap kemampuan yang dimiliki.",
             4
           ],
           [
             "Sedikit demi sedikit untuk menyelesaikan walau memakan waktu panjang.",
             1
           ],
           [
             "Berusaha bekerja sama dengan orang lain, agar penyelesaian bisa lebih cepat.",
             3
           ],
           [
             "Menyusun rencana agar penugasan dimaksud dapat diselesaikan dengan hasil yang baik.",
             5
           ],
           [
             "Berusaha mencari cara penyelesaian yang tidak membutuhkan waktu panjang.",
             2
           ]
         ],
         "jawaban_skor": [
           "Menyusun rencana agar penugasan dimaksud dapat diselesaikan dengan hasil yang baik.",
           5
         ]
       },
       {
         "id": 56,
         "soal": "Ketika gagal mencapai sesuatu yang saya inginkan, saya ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Melakukan introspeksi dan memperbaiki upaya.",
             5
           ],
           [
             "Mencari dengan skema siapa yang turut bertanggung jawab terhadap kegagalan saya tersebut.",
             2
           ],
           [
             "Bersedih hati.",
             1
           ],
           [
             "Meminta bantuan kepada lebih banyak orang.",
             3
           ],
           [
             "Mengambil waktu untuk menenangkan diri.",
             4
           ]
         ],
         "jawaban_skor": [
           "Melakukan introspeksi dan memperbaiki upaya.",
           5
         ]
       },
       {
         "id": 57,
         "soal": "Dalam rapat staf dan pimpinan, pendapat saya dkritik keras oleh peserta rapat lainnya. Respon saya adalah ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Menjelaskan kepada peserta rapat bahwa tidak ada alternatif solusi yang lebih baik daripada pendapat saya.",
             3
           ],
           [
             "Mencoba memperlajari kritikan tersebut dan menyempurnakan pendapat saya.",
             5
           ],
           [
             "Menerima kritikan tersebut sebagai masukan.",
             4
           ],
           [
             "Menjelaskan dengan detail alasan saya mengambil pendapat dimaksud.",
             2
           ],
           [
             "Mencoba sekuat tenaga mempertahankan pendapat saya.",
             1
           ]
         ],
         "jawaban_skor": [
           "Mencoba memperlajari kritikan tersebut dan menyempurnakan pendapat saya.",
           5
         ]
       },
       {
         "id": 58,
         "soal": "Saya ditawarkan oleh pimpinan untuk melanjutkan studi di luar negeri atas biaya kantor. Namun di sisi lain saya memahami bahwa kondisi keuangan kantor tidak dalam keadaan yang cukup baik. Maka keputusan saya adalah ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Menerima tawaran tersebut dan dengan berat hati membebani keuangan perusahaan.",
             1
           ],
           [
             "Menolak tawaran tersebut namun mohon kebijakan pimpinan jika ada kesempatan lagi saya akan diikutkan.",
             4
           ],
           [
             "Menerima tawaran tersebut, dengan permohonan penundaan, dan melanjutkan studi ketika kondisi keuangan membaik.",
             3
           ],
           [
             "Menerima tawaran tersebut dengan harapan akan meningkatkan kinerja yang dapat berkontribusi positif pada perusahaan dalam jangka panjang.",
             2
           ],
           [
             "Menolak tawaran tersebut, karena kondisi keuangan perusahaan adalah prioritas.",
             5
           ]
         ],
         "jawaban_skor": [
           "Menolak tawaran tersebut, karena kondisi keuangan perusahaan adalah prioritas.",
           5
         ]
       },
       {
         "id": 59,
         "soal": "Keluarga sedang mengalami permasalahan keuangan. Maka saya sebagai anak ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Seharusnya ayah dapat menindak tegas yang terlibat dalam masalah ini",
             3
           ],
           [
             "Tidak mempersoalkan masalah tersebut karena bukan urusan saya",
             2
           ],
           [
             "Saya akan menjaga kerahasiaan permasalahan yang terjadi dan mencoba memberikan alternatif solusi kepada ayah dan ibu",
             5
           ],
           [
             "Perlu membeberkan permasalahan kepada keluarga besar agar mendapat bantuan",
             1
           ],
           [
             "Pastikan bahwa ayah bertanggungjawab penuh terhadap masalah ini",
             4
           ]
         ],
         "jawaban_skor": [
           "Saya akan menjaga kerahasiaan permasalahan yang terjadi dan mencoba memberikan alternatif solusi kepada ayah dan ibu",
           5
         ]
       },
       {
         "id": 60,
         "soal": "Pimpinan lembaga menggelar rapat membahas penyusunan rencana kerja untuk tahun ajaran depan. Setiap peserta diharapkan mempersiapkan usulan. Respon saya ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Mungkin berminat untuk mengajukan suatu ide yang belum tentu dilaksanakan",
             4
           ],
           [
             "Tidak berminat sama sekali untuk mengajukan suatu ide apapun",
             1
           ],
           [
             "Berminat mengajukan suatu ide kegiatan yang akan dilaksanakan meskipun nantinya ide tersebut tidak diterima",
             5
           ],
           [
             "Ragu ragu untuk mengajukan suatu ide karena akan kecewa jika tidak diterima",
             2
           ],
           [
             "Akan mengajukan suatu ide jika diminta oleh pimpinan",
             3
           ]
         ],
         "jawaban_skor": [
           "Berminat mengajukan suatu ide kegiatan yang akan dilaksanakan meskipun nantinya ide tersebut tidak diterima",
           5
         ]
       },
       {
         "id": 61,
         "soal": "Yayasan tempat saya bekerja sedang mengalami masalah internal, sikap saya ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Saya tentu saja berusaha menjaga keamanan posisi saya agar tidak terusik",
             3
           ],
           [
             "Saya berusaha agar diri saya jangan sampai terkena imbasnya",
             2
           ],
           [
             "Saya berusaha memberikan gagasan pemecahan masalah kepada pimpinan, sambil menjaga kerahasiaan masalah internal ini",
             5
           ],
           [
             "Biarlah pimpinan yang mengambil keputusan",
             4
           ],
           [
             "Bagaimanapun juga publik berhak tahu, oleh karenanya saya beberkan masalah internal tersebut kepada publik",
             1
           ]
         ],
         "jawaban_skor": [
           "Saya berusaha memberikan gagasan pemecahan masalah kepada pimpinan, sambil menjaga kerahasiaan masalah internal ini",
           5
         ]
       },
       {
         "id": 62,
         "soal": "Saya ditugaskan untuk meminmpin tim kerja dengan batas waktu yang sangat ketat. Anggota tim kerja memperlihatkan sikap yang tidak peduli dengan tugas yang diemban. Sikap saya adalah ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Melaporkan mereka pada pimpinan agar diberi sanksi",
             2
           ],
           [
             "Membagi tugas secara adil dan memotivasi anggota untuk menyelesaikannya",
             5
           ],
           [
             "Bekerja sendiri yang penting tugas selesai",
             3
           ],
           [
             "Menasehati mereka agar sadar akan penyelesaiaan tugas yang diembannya",
             4
           ],
           [
             "Mengancam mengeluarkan anggota yang tidak serius dari tim kerja",
             1
           ]
         ],
         "jawaban_skor": [
           "Membagi tugas secara adil dan memotivasi anggota untuk menyelesaikannya",
           5
         ]
       },
       {
         "id": 63,
         "soal": "Andi, teman karib anda, melakukan kecurangan absensi. Maka anda ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Rekan kerja yang lain juga melakukannya, jadi tidaklah mengapa",
             1
           ],
           [
             "Mentoleransi sebab baru kali ini Andi melakukanya",
             2
           ],
           [
             "Mengingatkan dan menegur",
             4
           ],
           [
             "Menegur dan melaporkan apa adanya kepada atasan",
             5
           ],
           [
             "Menanyakan kepadanya mengapa dia melakukan hal tersebut",
             3
           ]
         ],
         "jawaban_skor": [
           "Menegur dan melaporkan apa adanya kepada atasan",
           5
         ]
       },
       {
         "id": 64,
         "soal": "Adik saya ingin melanjutkan studinya namun ada kendala biaya. Sedangkan orang tua saya sudah pensiun dan saya juga memiliki istri dan anak yang membutuhkan biaya. Maka saya ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Mencarikan biaya sekolah dengan berhutang",
             4
           ],
           [
             "Menyarankannya untuk memaksa orang tua mencari biaya dengan berhutang",
             1
           ],
           [
             "Memintanya mencari kerja dulu",
             3
           ],
           [
             "Menyarankan untuk mencari beasiswa",
             5
           ],
           [
             "Menyuruhnya mengurungkan niat",
             2
           ]
         ],
         "jawaban_skor": [
           "Menyarankan untuk mencari beasiswa",
           5
         ]
       },
       {
         "id": 65,
         "soal": "Jika dalam suatu rapat, rekan kantor memiliki pendapat yang berbeda, padahal andalah yang menjadi pemimpin rapat, maka ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Saya lihat dulu siapa dia",
             2
           ],
           [
             "Beda pendapat bukanlah masalah serius",
             4
           ],
           [
             "Saya pertimbangkan pendapat tersebut",
             5
           ],
           [
             "Saya teguh mempertahankan pendapat saya",
             3
           ],
           [
             "Menanyakannya kenapa dia berani berbeda pendapat dengan pemimpin rapat",
             1
           ]
         ],
         "jawaban_skor": [
           "Saya pertimbangkan pendapat tersebut",
           5
         ]
       },
       {
         "id": 66,
         "soal": "Jika salah seorang bawahan saya melakukan tugas yang saya berikan kepadanya dengan sangat baik, maka saya ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Berpura-pura tidak tahu akan keberhasilannya melakukan tugas",
             2
           ],
           [
             "Memuji setinggi langit agar dia juga senang kepada saya",
             1
           ],
           [
             "Puas, namun tak memuji karena hal itu akan membuatnya sombong",
             4
           ],
           [
             "Tak akan memuji",
             3
           ],
           [
             "Memuji secara proposional",
             5
           ]
         ],
         "jawaban_skor": [
           "Memuji secara proposional",
           5
         ]
       },
       {
         "id": 67,
         "soal": "Ketika sedang melakukan presentasi, kancing baju Pak Adhy, yang juga atasan anda, terlepas satu buah di bagian perut. Hal ini sangat mengganggu jalannya presentasi. Namun tampaknya tak ada yang berani memberitahu Pak Adhy. Bagaimana sikap anda?",
         "pembahasan": null,
         "pilihan": [
           [
             "Lebih baik saya diam karena yang lainpun juga diam saja",
             1
           ],
           [
             "Saya takut Pak Adhy tersinggung, jadi tak ada alasan untuk saya memberitahunya",
             2
           ],
           [
             "Saya berharap semoga Pak Adhy sadar dengan sendirinya",
             3
           ],
           [
             "Saya menuliskan ke secarik kertas mengenai hal tersebut lalu saya serahkan kepada Pak Adhy",
             5
           ],
           [
             "Meskipun hal itu mengganggu jalannya presentasi namun saya tak mau ambil resiko dengan memberitahunya",
             4
           ]
         ],
         "jawaban_skor": [
           "Saya menuliskan ke secarik kertas mengenai hal tersebut lalu saya serahkan kepada Pak Adhy",
           5
         ]
       },
       {
         "id": 68,
         "soal": "Saya mengajukan suatu usulan kepada atasan saya, namun usulan tersebut menurut atasan saya kurang tepat. Sikap saya adalah ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Ditolak bukanlah sesuatu yang baru bagi saya",
             2
           ],
           [
             "Mencari alternatif usulan lain yang lebih tepat",
             5
           ],
           [
             "Merasa sangat kecewa",
             1
           ],
           [
             "Saya bersikeras mencari upaya pembenaran terhadap usulan tersebut agar dia mau menerimanya",
             3
           ],
           [
             "Kecewa, namun berusaha melupakan hal tersebut",
             4
           ]
         ],
         "jawaban_skor": [
           "Mencari alternatif usulan lain yang lebih tepat",
           5
         ]
       },
       {
         "id": 69,
         "soal": "Kerja keras dan cermat merupakan wujud upaya untuk menjadi pribadi yang bermanfaat bagi organisasi. Berkaitan dengan hal tersebut, saya senang ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Pekerjaan yang menantang.",
             3
           ],
           [
             "Bekerja tanpa mengenal lelah dan pamrih.",
             2
           ],
           [
             "Bekerja dengan standar hasil yang tinggi.",
             5
           ],
           [
             "Pekerjaan rutin.",
             1
           ],
           [
             "Pekerjaan yang menumbuhkan kreatifitas baru.",
             4
           ]
         ],
         "jawaban_skor": [
           "Bekerja dengan standar hasil yang tinggi.",
           5
         ]
       },
       {
         "id": 70,
         "soal": "Jika suatu rencana kerja terlihat rumit, maka ...",
         "pembahasan": null,
         "pilihan": [
           [
             "Saya minta pendapat dari orang lain yang lebih berpengalaman.",
             4
           ],
           [
             "Saya berani mencoba setelah mempertimbangkan risikonya.",
             2
           ],
           [
             "Yang penting saya coba dulu.",
             3
           ],
           [
             "Saya khawatir jika mencobanya dan gagal.",
             1
           ],
           [
             "Saya akan memetakannya terlebih dahulu, untuk memutuskan langkah-langkah yang harus dilakukan.",
             5
           ]
         ],
         "jawaban_skor": [
           "Saya akan memetakannya terlebih dahulu, untuk memutuskan langkah-langkah yang harus dilakukan.",
           5
         ]
       }
     ],
     "soal_tiu": [
       {
         "id": 1,
         "soal": "ADALAT =",
         "jawaban": "keadilan",
         "pembahasan": "Adalat = Keadilan\n Adalat adalah kata serapan dari Bahasa arab yang padanan kata ataupun padanan pengertiannya lebih dekat dengan keadilan ataupun kebajikan.",
         "pilihan": [
           [
             "kemanusiaan",
             0
           ],
           [
             "kedaulatan",
             0
           ],
           [
             "keadilan",
             5
           ],
           [
             "kesopanan",
             0
           ],
           [
             "kebijaksanaan",
             0
           ]
         ],
         "jawaban_skor": [
           "keadilan",
           5
         ]
       },
       {
         "id": 2,
         "soal": "CAKAK =",
         "jawaban": "perkelahian",
         "pembahasan": "Cakak = Perkelahian\n Cakak adalah kata serapan dari Bahasa Minangkabau yang padanan kata ataupun padanan pengertiannya lebih dekat dengan perkelahian",
         "pilihan": [
           [
             "perlagaan",
             0
           ],
           [
             "pertandingan",
             0
           ],
           [
             "perkelahian",
             5
           ],
           [
             "perlombaan",
             0
           ],
           [
             "pertempuran",
             0
           ]
         ],
         "jawaban_skor": [
           "perkelahian",
           5
         ]
       },
       {
         "id": 3,
         "soal": "HIBRIDA =",
         "jawaban": "Bibit unggul",
         "pembahasan": "Hibrida = turunan yg dihasilkan dr perkawinan antara dua jenis yg berlainan (tentang hewan atau tumbuhan); bibit unggul.",
         "pilihan": [
           [
             "Tanaman rendah",
             0
           ],
           [
             "Antar tanaman",
             0
           ],
           [
             "Cepat berubah",
             0
           ],
           [
             "Bibit unggul",
             5
           ],
           [
             "Cangkokan",
             0
           ]
         ],
         "jawaban_skor": [
           "Bibit unggul",
           5
         ]
       },
       {
         "id": 4,
         "soal": "INDUKSI >< ...",
         "jawaban": "Deduksi",
         "pembahasan": "Lawan kata yang tepat untuk kata induksi adalah kata deduksi.",
         "pilihan": [
           [
             "Konveksi",
             0
           ],
           [
             "Reduksi",
             0
           ],
           [
             "Friksi",
             0
           ],
           [
             "Fusi",
             0
           ],
           [
             "Deduksi",
             5
           ]
         ],
         "jawaban_skor": [
           "Deduksi",
           5
         ]
       },
       {
         "id": 5,
         "soal": "JAM : ... = KALENDER : HARI",
         "jawaban": "Detik",
         "pembahasan": "Kalender tersusun dari kumpulan hari.",
         "pilihan": [
           [
             "Masehi",
             0
           ],
           [
             "Tahun",
             0
           ],
           [
             "Jam",
             0
           ],
           [
             "Detik",
             5
           ],
           [
             "Waktu",
             0
           ]
         ],
         "jawaban_skor": [
           "Detik",
           5
         ]
       },
       {
         "id": 6,
         "soal": "INDONESIA : YOGYAKARTA = . . .",
         "jawaban": "Uni Emirat Arab : Dubai",
         "pembahasan": "Salah satu kota besar di Indonesai adalah Yogyakarta, Salah satu kota besar di Uni Emirat Arab adalah Dubai.",
         "pilihan": [
           [
             "Uni Emirat Arab : Dubai",
             5
           ],
           [
             "Kalimantan : Samarinda",
             0
           ],
           [
             "Malaysia : Sarawak",
             0
           ],
           [
             "Papua : Merauke",
             0
           ],
           [
             "Afganistan : Yaman",
             0
           ]
         ],
         "jawaban_skor": [
           "Uni Emirat Arab : Dubai",
           5
         ]
       },
       {
         "id": 7,
         "soal": "TIMUR : SELATAN : TENGGARA = . . .",
         "jawaban": "Barat : Utara : Barat laut",
         "pembahasan": "Barat : Utara : Barat laut. Yang merupakan hubungan arah mata angin.",
         "pilihan": [
           [
             "Selatan : Barat : Barat daya",
             0
           ],
           [
             "Barat : Utara : Barat laut",
             5
           ],
           [
             "Jelas : Pasti : Tidak mungkin",
             0
           ],
           [
             "Timur : Barat : Utara",
             0
           ],
           [
             "Pasti : Tidak mungkin : Mungkin",
             0
           ]
         ],
         "jawaban_skor": [
           "Barat : Utara : Barat laut",
           5
         ]
       },
       {
         "id": 8,
         "soal": "RADIAN : . . . = . . . : GERAK",
         "jawaban": "sudut - gaya",
         "pembahasan": "Sudut-Gaya. Hubungan persamaan arti atau makna.",
         "pilihan": [
           [
             "astra - olah raga",
             0
           ],
           [
             "ban - mobil",
             0
           ],
           [
             "system – roda",
             0
           ],
           [
             "sudut - gaya",
             5
           ],
           [
             "lingkaran - diam",
             0
           ]
         ],
         "jawaban_skor": [
           "sudut - gaya",
           5
         ]
       },
       {
         "id": 9,
         "soal": "PRESTASI : HASIL = PRODUKSI : ...",
         "jawaban": "Usaha",
         "pembahasan": "Prestasi adalah suatu hasil yang dicapai.\n Produksi adalah suatu usaha yang dilakukan.",
         "pilihan": [
           [
             "Penawaran",
             0
           ],
           [
             "Harga",
             0
           ],
           [
             "Penjualan",
             0
           ],
           [
             "Usaha",
             5
           ],
           [
             "Prestasi",
             0
           ]
         ],
         "jawaban_skor": [
           "Usaha",
           5
         ]
       },
       {
         "id": 10,
         "soal": "Tanggul sungai Pelalangan di Desa Plosowahyu Kecamatan Lamongan Jawa Timur, Senin malam (15/12/19) jebol setelah diterjang banjir akibat hujan deras yang mengguyur wilayah Lamongan Selatan. Tanggul sungai jebol sepanjang 10 meter sehingga membuat air menggenangi puluhan hektar lahan tambak dan tanaman padi di Kecamatan Lamongan dan Kecamatan Turi. \n Menurut salah seorang petani, tanggul mulai jebol sekitar jam 09.00 malam akibat luapan sungai Pelalangan yang terus meluber. Jebolnya tanggul ini merupakan yang kedua kalinya terjadi. Bahkan, tahun lalu rumah warga juga sempat terendam banjir. \n Para petani di Desa Plosowahyu merasa resah karena sejumlah titik tanggul di desanya juga mulai retak-retak-retak dan rawan jebol. Mereka hanya berharap pemerintah Kabupaten Lamongan segera memperbaiki tanggul agar petani tidak terus merugi. Akibat banjir yang merendam lahan tambak dan tanaman padi ini, petani mengalami kerugian hingga jutaan rupiah. \nSudah berapa kali tanggul itu jebol?",
         "jawaban": "dua",
         "pembahasan": "Tersurat jelas pada paragraf kedua.",
         "pilihan": [
           [
             "empat",
             0
           ],
           [
             "sering",
             0
           ],
           [
             "satu",
             0
           ],
           [
             "tidak pernah",
             0
           ],
           [
             "dua",
             5
           ]
         ],
         "jawaban_skor": [
           "dua",
           5
         ]
       },
       {
         "id": 11,
         "soal": "Informasi di bawah ini sesuai dengan berita di atas, kecuali...",
         "jawaban": "tanggul sungai jebol sepanjang 10 meter sehingga membuat air menggenangi puluhan hektar lahan tambak dan tanaman padi di Kecamatan Lamongan dan Kecamatan Cimahi.",
         "pembahasan": "Sudah jelas, ada di teks. Ketidaksesuaian terletak pada nama Kecamatannya.",
         "pilihan": [
           [
             "jebolnya tanggul ini bukan yang pertama kalinya terjadi.",
             0
           ],
           [
             "menurut salah seorang petani, tanggul mulai jebol sekitar jam 09.00 malam akibat luapan sungai Pelalangan yang terus meluber.",
             0
           ],
           [
             "akibat banjir yang merendam lahan tambak dan tanaman padi ini, petani mengalami kerugian hingga jutaan rupiah.",
             0
           ],
           [
             "tanggul sungai jebol sepanjang 10 meter sehingga membuat air menggenangi puluhan hektar lahan tambak dan tanaman padi di Kecamatan Lamongan dan Kecamatan Cimahi.",
             5
           ],
           [
             "para petani di Desa Plosowahyu merasa resah karena sejumlah titik tanggul di desanya juga mulai retak-retak-retak dan rawan jebol.",
             0
           ]
         ],
         "jawaban_skor": [
           "tanggul sungai jebol sepanjang 10 meter sehingga membuat air menggenangi puluhan hektar lahan tambak dan tanaman padi di Kecamatan Lamongan dan Kecamatan Cimahi.",
           5
         ]
       },
       {
         "id": 12,
         "soal": "Pak Drajad berbelanja dengan menggunakan mobil yang memiliki daya angkut 750 kg. Pak Drajad membeli 3 kuintal gula dan 7 karung beras yang masing-masing karung beras beratnya 50kg. Berat maksimal tepung terigu yang dapat dibelinya?",
         "jawaban": "100 kg",
         "pembahasan": "750 kg = 3 kuintal gula + (7 × 50 kg beras) + tepung terigu.\n Misalkan berat tepung terigu = a\n Persamaan matematisnya menjadi:\n 750 = (3 × 100) + 350 + a\n  a = 750 – 300 – 350\n  = 100\n Jadi, berat maksimum tepung terigu yang dapat dibeli Pak Drajad adalah 100 kg.",
         "pilihan": [
           [
             "100 kg",
             5
           ],
           [
             "115 kg",
             0
           ],
           [
             "125 kg",
             0
           ],
           [
             "50 kg",
             0
           ],
           [
             "75 kg",
             0
           ]
         ],
         "jawaban_skor": [
           "100 kg",
           5
         ]
       },
       {
         "id": 13,
         "soal": "Sebuah perusahaan melakukan efisiensi di berbagai bidang. Sebanyak 20% dari karyawannya tidak diperpanjang kontraknya dan untuk meningkatkan kinerja dari karyawan yang diperpanjang kontraknya, gajinya dinaikkan 15%. Berapa efisiensi perusahaan jika dilihat dari pengeluaran untuk gaji?",
         "jawaban": "0.08",
         "pembahasan": "Misalkan banyaknya karyawan = C\n Gaji masing-masing karyawan = D\n Jadi gaji seluruh karyawan = CD\n 20% karyawan tidak perpanjang kontraknya, berarti jumlah karyawan tinggal 80% = 0,8 C\n Gaji untuk karyawan sebanyak 0,8 C dinaikkan 15%:\n = 0,8C × (D + 15%D)\n = 0,8C × (D + 0,15D)\n = 0,8C × 1,15D\n = 0,92CD\n Efisiensi = CD – 0,92 CD\n  = 0,8 CD\n  = 8%",
         "pilihan": [
           [
             "0.1",
             0
           ],
           [
             "0.08",
             5
           ],
           [
             "0.12",
             0
           ],
           [
             "0.17",
             0
           ],
           [
             "0.15",
             0
           ]
         ],
         "jawaban_skor": [
           "0.08",
           5
         ]
       },
       {
         "id": 14,
         "soal": "Sebuah jam setiap hari terlambat 15 menit. Berapa hari yang diperlukan oleh jam tersebut untuk sampai/ kembali pada titik ketika jam tersebut menunjukkan waktu yang benar?",
         "jawaban": "96",
         "pembahasan": "Tiap hari terlambat 15 menit\n 1 hari = 24 jam × 60 menit = 1440 menit\n Waktu yang diperlukan untuk kembali menunjukkan waktu yang benar:\n = 1440 menit : 15 menit = 96 hari.",
         "pilihan": [
           [
             "80",
             0
           ],
           [
             "96",
             5
           ],
           [
             "72",
             0
           ],
           [
             "82",
             0
           ],
           [
             "36",
             0
           ]
         ],
         "jawaban_skor": [
           "96",
           5
         ]
       },
       {
         "id": 15,
         "soal": "Sebuah pabrik menyediakan arang batu untuk memesang 6 buah ketel selama 4 minggu. Berapa lamakah pabrik itu harus menyediakan arang batu supaya dapat dipakai untuk memanaskan 18 buah ketel?",
         "jawaban": "12 minggu",
         "pembahasan": "Banyaknya ketel Waktu\n 6 buah 4 minggu\n 18 buah x minggu\n \n Perbandingan 4 : 6 = x : 18\n 4/6=x/18\n 6x = 72\n  x = 12 minggu.",
         "pilihan": [
           [
             "15 minggu",
             0
           ],
           [
             "14 minggu",
             0
           ],
           [
             "12 minggu",
             5
           ],
           [
             "24 minggu",
             0
           ],
           [
             "3 minggu",
             0
           ]
         ],
         "jawaban_skor": [
           "12 minggu",
           5
         ]
       },
       {
         "id": 16,
         "soal": "Ayah membagi uang kepada 4 orang anaknya. Anak yang pertama mendapat sejumlah uang. Anak kedua mendapat sepertiga dari sisa uang. Anak ketiga mendapat dua pertiga dari anak pertama, dan yang keempat sisanya, yaitu dua perlima dari pembagian yang pertama. Jumlah uang yang dibagikan ayah adalah Rp 52.000. berapa yang diperoleh anak pertama?",
         "jawaban": "Rp20.000",
         "pembahasan": "Misalkan jumlah uang seluruhnya = y\n Yang didapat anak pertama = x\n Yang didapat anak kedua = 1/3 (y - x)\n Yang didapat anak ketiga = 2/3 x\n Yang didapat anak keempat\n =2/5 x=y-x-1/3 (y-x)-2/3 x\n 2/5 x=y-x-1/3 y+1/3x-2/3x\n 2/5 x=2/3 y-4/3 x\n 2/5 x+4/3 x=2/3 y\n 6/15 x+20/15 x=10/15 y\n 26/15 x=10/15 x\n x=10/15 ∶ 26/15 y\n =10/15×15/26 y\n =10/26 y\n Jadi anak pertama mendapat\n = 10/26 × Rp 52.000 = Rp 20.000.",
         "pilihan": [
           [
             "Rp20.000",
             5
           ],
           [
             "Rp27.000",
             0
           ],
           [
             "Rp24.000",
             0
           ],
           [
             "Rp50.000",
             0
           ],
           [
             "Rp23.000",
             0
           ]
         ],
         "jawaban_skor": [
           "Rp20.000",
           5
         ]
       },
       {
         "id": 17,
         "soal": "Fani berjalan dari kota A ke kota B dengan kecepatan 8 km/jam selama 5 jam dan ia kembali ke kota A dengan naik motor. Berapakah kecepatan rata-rata seluruh perjalanan Fani bila ia kembali dari kota B ke kota A selama 3 jam?",
         "jawaban": "10 km/jam.",
         "pembahasan": "Lama perjalanan dari kota A ke kota B dengan kecepatan 8 km/jam adalah 5 jam. Jika lama waktu perjalanan dari kota A ke kota B adalah 3 jam, kecepatan perjalanan tersebut:\n vAB × tAB = vBA × tBA\n  8 × 5 = vBA × 3\n  vBA = 40/3 km/jam\n kecepatan rata-rata perjalanan Adi (v):\n v=(vAB+vBA)/3\n =(8+40/3)/2\n =(24/3+40/3)/2\n =(64/3)/2\n =10 2/3 km/jam\n Yang paling mendekati adalah 10 km/jam.",
         "pilihan": [
           [
             "10 km/jam.",
             5
           ],
           [
             "8 km/jam.",
             0
           ],
           [
             "9 km/jam.",
             0
           ],
           [
             "5 km/jam.",
             0
           ],
           [
             "12 km/jam.",
             0
           ]
         ],
         "jawaban_skor": [
           "10 km/jam.",
           5
         ]
       },
       {
         "id": 18,
         "soal": "Berapa lama waktu yang diperlukan untuk mengisi penuh air ke dalam sebuah tangki berkapasitas 3.750 sentimeter kubik jika air tersebut dipompakan ke dalam tangki dengan kecepatan 800 sentimeter kubik per menit, namun selang dimaksud ada kebocoran 300 sentimeter kubik per menit?",
         "jawaban": "7 menit 30 detik",
         "pembahasan": "3.750/((800-300)) = 7,5 menit = 7 menit 30 detik.",
         "pilihan": [
           [
             "8 menit",
             0
           ],
           [
             "6 menit",
             0
           ],
           [
             "5 menit 25 detik",
             0
           ],
           [
             "3 menit 36 detik",
             0
           ],
           [
             "7 menit 30 detik",
             5
           ]
         ],
         "jawaban_skor": [
           "7 menit 30 detik",
           5
         ]
       },
       {
         "id": 19,
         "soal": "Toni bekerja selama 6 jam setiap hari mulai pukul 08.00, tetapi pada hari ini ia bekerja sampai pukul 15.15. Berapa menit lebih lamakah ia bekerja pada hari ini?",
         "jawaban": "75 menit.",
         "pembahasan": "Toni mulai bekerja pada pukul 08.00 dan setiap hari bekerja selama 6 jam, berarti pekerjaanya akan berakhir pada:\n Pukul 08.00 + 6 jam = 14.00\n Hari ini Toni pulang pada pukul 15.15, berarti ada kelebihan jam kerja selama:\n = pukul 15.15 – pukul 14.00\n = 1 jam 15 menit\n = 75 menit.",
         "pilihan": [
           [
             "135 menit.",
             0
           ],
           [
             "125 menit.",
             0
           ],
           [
             "75 menit.",
             5
           ],
           [
             "85 menit.",
             0
           ],
           [
             "15 menit.",
             0
           ]
         ],
         "jawaban_skor": [
           "75 menit.",
           5
         ]
       },
       {
         "id": 20,
         "soal": "Keluarga Pak Anwar akan berfoto bersama di sebuah studio foto. Pak Anwar mempunyai 2 orang anak. Mereka semua akan berfoto secra berdampingan. Peluang foto pak Anwar dan istri berdampingan adalah ...",
         "jawaban": "½",
         "pembahasan": "Peluang =",
         "pilihan": [
           [
             "2020-12-01 00:00:00",
             0
           ],
           [
             "2020-06-01 00:00:00",
             0
           ],
           [
             "2020-05-01 00:00:00",
             0
           ],
           [
             "2020-03-01 00:00:00",
             0
           ],
           [
             "½",
             5
           ]
         ],
         "jawaban_skor": [
           "½",
           5
         ]
       },
       {
         "id": 21,
         "soal": "Sebotol sirup dapat dibuat 80 gelas minuman jika dilarutkan dalam air dengan perbandingan 1 bagian sirup untuk 4 bagian air. Berapa gelas minuman yang diperoleh dari sebotol sirup jika perbandingan larutannya 1 bagian sirup untuk 5 bagian air?",
         "jawaban": "100 gelas.",
         "pembahasan": "v 1 sirup : 4 air = 20 gelas bagian sirup : 80 gelas bagian air.\n v 1 sirup : 5 air = 20 gelas bagian sirup : 100 gelas bagian air.",
         "pilihan": [
           [
             "84 gelas.",
             0
           ],
           [
             "92 gelas.",
             0
           ],
           [
             "96 gelas.",
             0
           ],
           [
             "100 gelas.",
             5
           ],
           [
             "80 gelas.",
             0
           ]
         ],
         "jawaban_skor": [
           "100 gelas.",
           5
         ]
       },
       {
         "id": 22,
         "soal": "2, 5, 4, 6, 6, 7, 8, 8, ...",
         "jawaban": "10",
         "pembahasan": "FREE001.png",
         "pilihan": [
           [
             "12",
             0
           ],
           [
             "8",
             0
           ],
           [
             "9",
             0
           ],
           [
             "10",
             5
           ],
           [
             "7",
             0
           ]
         ],
         "jawaban_skor": [
           "10",
           5
         ]
       },
       {
         "id": 23,
         "soal": "11, 14, 19, 26, 35, 38, 43, 50, ...",
         "jawaban": "59",
         "pembahasan": "FREE002.png",
         "pilihan": [
           [
             "55",
             0
           ],
           [
             "53",
             0
           ],
           [
             "59",
             5
           ],
           [
             "58",
             0
           ],
           [
             "57",
             0
           ]
         ],
         "jawaban_skor": [
           "59",
           5
         ]
       },
       {
         "id": 24,
         "soal": "12, 15, 20, 27, 36, 47, 60, 75, ..., ...",
         "jawaban": "92, 111",
         "pembahasan": "FREE003.png",
         "pilihan": [
           [
             "96, 117",
             0
           ],
           [
             "92, 111",
             5
           ],
           [
             "86, 97",
             0
           ],
           [
             "102, 125",
             0
           ],
           [
             "80, 95",
             0
           ]
         ],
         "jawaban_skor": [
           "92, 111",
           5
         ]
       },
       {
         "id": 25,
         "soal": "ua apel dari kota M harum baunya.\n Sebagian yang harum baunya rasanya manis.",
         "jawaban": "Sebagian apel dari kota M tak manis.",
         "pembahasan": "Dengan hukum silogisme maka sebagian apel dari kota M rasanya manis, artinya sebagian yang lain tidak manis.",
         "pilihan": [
           [
             "Sebagian apel dari kota M tak manis.",
             5
           ],
           [
             "Taka da apel dari kota M yang manis.",
             0
           ],
           [
             "Semua apel yang manis berasal dari kota M.",
             0
           ],
           [
             "Semua apel dari kota M manis.",
             0
           ],
           [
             "Sebagian yang manis bukan apel.",
             0
           ]
         ],
         "jawaban_skor": [
           "Sebagian apel dari kota M tak manis.",
           5
         ]
       },
       {
         "id": 26,
         "soal": "ua yang bulat adalah biji.\n Sebagian biji rasanya pahit.",
         "jawaban": "Sebagian yang bulat rasanya tidak pahit.",
         "pembahasan": "Berdasar hukum silogisme maka kesimpulannya adalah sebagian yang bulat rasanya pahit. Artinya, sebagian yang lain rasanya tidak pahit.",
         "pilihan": [
           [
             "Sebagian yang bulat rasanya tidak pahit.",
             5
           ],
           [
             "Semua yang bulat rasanya tidak pahit.",
             0
           ],
           [
             "Semua yang rasanya tidak pahit tidak bulat.",
             0
           ],
           [
             "Semua biji adalah bulat.",
             0
           ],
           [
             "Semua yang rasanya tidak pahit adalah biji.",
             0
           ]
         ],
         "jawaban_skor": [
           "Sebagian yang bulat rasanya tidak pahit.",
           5
         ]
       },
       {
         "id": 27,
         "soal": "Semua penerima beasiswa berprestasi istimewa. Beberapa siswa menerima beasiswa.",
         "jawaban": "Beberapa siswa berprestasi istimewa.",
         "pembahasan": "Dengan hukum silogisme maka beberapa siswa berprestasi istimewa.",
         "pilihan": [
           [
             "Beberapa siswa berprestasi istimewa.",
             5
           ],
           [
             "Asa siswa penerima beasiswa yang tidak berprestasi istimewa.",
             0
           ],
           [
             "Semua siswa berprestasi istimewa mendapat beasiswa.",
             0
           ],
           [
             "Tak ada siswa berprestasi istimewa yang tak menerima beasiswa.",
             0
           ],
           [
             "Tak ada siswa berprestasi istimewa yang menerima beasiswa.",
             0
           ]
         ],
         "jawaban_skor": [
           "Beberapa siswa berprestasi istimewa.",
           5
         ]
       },
       {
         "id": 28,
         "soal": "Faris rajin belajar atau dia adalah siswa tidak berkaca mata di kelas. Pernyataan inversnya sebagai berikut:",
         "jawaban": "Faris rajin belajar maka faris siswa berkaca mata di kelas.",
         "pembahasan": "v Faris tidak rajin belajar= p\n v siswa tidak berkaca mata di kelas = q\n v ~p v q = p →q invernya adalah ~p →~q = Faris rajin belajar maka faris siswa berkaca mata di kelas",
         "pilihan": [
           [
             "Faris rajin belajar dan dia berkaca mata dikelas.",
             0
           ],
           [
             "Faris rajin belajar dan berkaca mata dikelas.",
             0
           ],
           [
             "Faris rajin belajar maka faris siswa berkaca mata di kelas.",
             5
           ],
           [
             "Jika faris tidak berkaca mata di kelas maka faris rajin belajar.",
             0
           ],
           [
             "Jika faris berkaca mata di kelas maka faris rajin belajar.",
             0
           ]
         ],
         "jawaban_skor": [
           "Faris rajin belajar maka faris siswa berkaca mata di kelas.",
           5
         ]
       },
       {
         "id": 29,
         "soal": "Semua orang suka makan, sebagian orang tidak suka minum kopi, sebagian orang yang tidak suka minum kopi suka minum teh.",
         "jawaban": "Sebagian orang yang suka makan suka minum teh",
         "pembahasan": "FREE004.png",
         "pilihan": [
           [
             "Sebagian orang yang suka makan suka minum teh",
             5
           ],
           [
             "Sebagian orang yang suka makan suka minum juz",
             0
           ],
           [
             "Sebagian orang yang suka minum teh suka minum kopi",
             0
           ],
           [
             "Semua orang yang suka minum kopi suka minum teh",
             0
           ],
           [
             "Semua orang yang suka minum kopi suka minum jus",
             0
           ]
         ],
         "jawaban_skor": [
           "Sebagian orang yang suka makan suka minum teh",
           5
         ]
       },
       {
         "id": 30,
         "soal": "Bu Ani memiliki 4 (empat) orang anak, yaitu Budi, Badu, Sara dan Sari. Mereka berempat memiliki kesukaan yang berbeda terhadap mata pelajaran tertentu. Budi dan Sara menyukai mata pelajaran Matematika dan Bahasa Indonesia. Badu dan Sari menyukai menyukai Fisika dan Bahasa Inggris. Budi dan Sari sama-sama menyukai pelajaran Geografi dan Ekonomi. Badu dan Sara juga suka pelajaran Sejarah dan Olahraga. Mata pelajaran apa yang tidak disukai Sara?",
         "jawaban": "Bahasa inggris",
         "pembahasan": "v Budi : matematika, bahasa Indonesia, geografi, ekonomi\n v Badu : fisika, bahasa inggris, sejarah, olahraga\n v Sara : matematika, bahasa Indonesia, sejarah, olahraga\n v Sari : fisika, bahasa inggris, geografi, ekonomi.\nYang menyukai mata pelajaran bahasa inggris hanyalah Badu dan Sari",
         "pilihan": [
           [
             "Sejarah",
             0
           ],
           [
             "Bahasa inggris",
             5
           ],
           [
             "Matematika",
             0
           ],
           [
             "Olahraga",
             0
           ],
           [
             "Bahasa indonesia",
             0
           ]
         ],
         "jawaban_skor": [
           "Bahasa inggris",
           5
         ]
       },
       {
         "id": 31,
         "soal": "Bu Ani memiliki 4 (empat) orang anak, yaitu Budi, Badu, Sara dan Sari. Mereka berempat memiliki kesukaan yang berbeda terhadap mata pelajaran tertentu. Budi dan Sara menyukai mata pelajaran Matematika dan Bahasa Indonesia. Badu dan Sari menyukai menyukai Fisika dan Bahasa Inggris. Budi dan Sari sama-sama menyukai pelajaran Geografi dan Ekonomi. Badu dan Sara juga suka pelajaran Sejarah dan Olahraga. Siapa yang suka pelajaran fisika dan sejarah?",
         "jawaban": "Badu",
         "pembahasan": "v Budi : matematika, bahasa Indonesia, geografi, ekonomi\n v Badu : fisika, bahasa inggris, sejarah, olahraga\n v Sara : matematika, bahasa Indonesia, sejarah, olahraga\n v Sari : fisika, bahasa inggris, geografi, ekonomi. \nJawabannya adalah Badu, karena meskipun Sari menyukai fisika namun ia tidak suka sejarah. Sedangkan Sara tidak mneyukai fisika dan Budi tidak menyukai kedua mata pelajaran tersebut.",
         "pilihan": [
           [
             "Badu dan Sara",
             0
           ],
           [
             "Sara",
             0
           ],
           [
             "Badu",
             5
           ],
           [
             "Budi",
             0
           ],
           [
             "Sari",
             0
           ]
         ],
         "jawaban_skor": [
           "Badu",
           5
         ]
       },
       {
         "id": 32,
         "soal": "Bu Ani memiliki 4 (empat) orang anak, yaitu Budi, Badu, Sara dan Sari. Mereka berempat memiliki kesukaan yang berbeda terhadap mata pelajaran tertentu. Budi dan Sara menyukai mata pelajaran Matematika dan Bahasa Indonesia. Badu dan Sari menyukai menyukai Fisika dan Bahasa Inggris. Budi dan Sari sama-sama menyukai pelajaran Geografi dan Ekonomi. Badu dan Sara juga suka pelajaran Sejarah dan Olahraga. Siapa yang suka pelajaran matematika dan ekonomi namun tidak suka olahraga?",
         "jawaban": "Budi",
         "pembahasan": "v Budi : matematika, bahasa Indonesia, geografi, ekonomi\n v Badu : fisika, bahasa inggris, sejarah, olahraga\n v Sara : matematika, bahasa Indonesia, sejarah, olahraga\n v Sari : fisika, bahasa inggris, geografi, ekonomi\nYang suka pelajaran matematika dan ekonomi namun tidak suka olahraga adalah Budi.",
         "pilihan": [
           [
             "Budi dan Sara",
             0
           ],
           [
             "Budi",
             5
           ],
           [
             "Badu dan Sari",
             0
           ],
           [
             "Sara",
             0
           ],
           [
             "Badu",
             0
           ]
         ],
         "jawaban_skor": [
           "Budi",
           5
         ]
       },
       {
         "id": 33,
         "soal": "Bu Ani memiliki 4 (empat) orang anak, yaitu Budi, Badu, Sara dan Sari. Mereka berempat memiliki kesukaan yang berbeda terhadap mata pelajaran tertentu. Budi dan Sara menyukai mata pelajaran Matematika dan Bahasa Indonesia. Badu dan Sari menyukai menyukai Fisika dan Bahasa Inggris. Budi dan Sari sama-sama menyukai pelajaran Geografi dan Ekonomi. Badu dan Sara juga suka pelajaran Sejarah dan Olahraga. Mata pelajaran apa saja yang disukai oleh Budi?",
         "jawaban": "matematika, bahasa Indonesia, geografi, ekonomi",
         "pembahasan": "v Budi : matematika, bahasa Indonesia, geografi, ekonomi\n v Badu : fisika, bahasa inggris, sejarah, olahraga\n v Sara : matematika, bahasa Indonesia, sejarah, olahraga\n v Sari : fisika, bahasa inggris, geografi, ekonomi. \nMata Pelajaran yang disukai Budi adalah matematika, bahasa inggris, geografi, ekonomi.",
         "pilihan": [
           [
             "matematika, bahasa Indonesia, geografi, ekonomi",
             5
           ],
           [
             "olahraga, sejarah, matematika, bahasa Indonesia",
             0
           ],
           [
             "matematika, bahasa inggris, geografi dan ekonomi",
             0
           ],
           [
             "matematika, bahasa Indonesia, geografi, sejarah",
             0
           ],
           [
             "fisika, bahasa inggris, geografi dan ekonomi",
             0
           ]
         ],
         "jawaban_skor": [
           "matematika, bahasa Indonesia, geografi, ekonomi",
           5
         ]
       },
       {
         "id": 34,
         "soal": "FREE005.png",
         "jawaban": "FREE009.png",
         "pembahasan": "Kubus D adalah kubus yang dapat terbentuk dari kerangka kubus di atasnya.",
         "pilihan": [
           [
             "FREE009.png",
             5
           ],
           [
             "FREE007.png",
             0
           ],
           [
             "FREE008.png",
             0
           ],
           [
             "FREE010.png",
             0
           ],
           [
             "FREE006.png",
             0
           ]
         ],
         "jawaban_skor": [
           "FREE009.png",
           5
         ]
       },
       {
         "id": 35,
         "soal": "FREE011.png",
         "jawaban": "FREE014.png",
         "pembahasan": "Garis lurus berubah menjadi garis bengkok, begitu pula sebaliknya.",
         "pilihan": [
           [
             "FREE015.png",
             0
           ],
           [
             "FREE016.png",
             0
           ],
           [
             "FREE012.png",
             0
           ],
           [
             "FREE013.png",
             0
           ],
           [
             "FREE014.png",
             5
           ]
         ],
         "jawaban_skor": [
           "FREE014.png",
           5
         ]
       }
     ],
     "soal_twk": [
       {
         "id": 31,
         "soal": "Bu Mayra adalah pejabat pada Kementerian Hukum dan HAM, di lingkungan kantornya Bu Mayra adalah pejabat yang sangat disegani, hal ini karena integritas yang selalu dipegang teguh oleh Bu Mayra, selama menjalani dua puluh tahun masa kerjanya Bu Mayra selalu menegakkan kebenaran dan keadilan, tidak sekalipun hal-hal yang melunturkan integritas dilakukan oleh Bu Mayra. Kutipan wacana dimaksud mencerminkan nilai Pancasila yang dipegang teguh, khususnya…",
         "jawaban": "Sila Kedua.",
         "pembahasan": "Penekanan dalam wacana diatas adalah sikap yang berani membela kebenaran dan keadilan, hal ini sejalan dengan butir-butir Pancasila Sila Kedua.",
         "pilihan": [
           [
             "Sila Keempat.",
             0
           ],
           [
             "Sila Kedua.",
             5
           ],
           [
             "Sila Ketiga.",
             0
           ],
           [
             "Sila Pertama.",
             0
           ],
           [
             "Sila Kelima.",
             0
           ]
         ],
         "jawaban_skor": [
           "Sila Kedua.",
           5
         ]
       },
       {
         "id": 32,
         "soal": "Kasus intoleransi di Indonesia, menjadi salah satu topik yang selalu menarik untuk dibahas. Sebagaimana kita ketahui bahwa negara kita merupakan negara yang sangat beragam baik dari segi budaya, suku hingga agama yang berbeda-beda. Tentunya toleransi menjadi salah satu bagian yang sudah sangat akrab dan menjadi cikal bakal dari lahirnya bangsa Indonesia. Berkebalikan dari itu, munculnya paham intoleransi menjadi salah satu hal wajib untuk menjadi perhatian, terutama intoleransi dalam bidang keagamaan. Untuk terhindar dari hal tersebut kita harus memahami Pancasila, khususnya…",
         "jawaban": "Sila Pertama.",
         "pembahasan": "Penekanan dalam wacana diatas adalah sikap intoleransi dalam keagamaan. Untuk terhindar dari hal tersebut maka kita harus mengembangkan sikap hormat-menghormati dan bekerjasama anatra pemeluk agama dengan penganut kepercayaan yang berbeda-beda terhadap Tuhan Yang Maha Esa, hal ini sejalan dengan butir-butir Pancasila Sila Pertama.",
         "pilihan": [
           [
             "Sila Pertama.",
             5
           ],
           [
             "Sila Ketiga.",
             0
           ],
           [
             "Sila Kedua.",
             0
           ],
           [
             "Sila Kelima.",
             0
           ],
           [
             "Sila Keempat.",
             0
           ]
         ],
         "jawaban_skor": [
           "Sila Pertama.",
           5
         ]
       },
       {
         "id": 33,
         "soal": "Radikalisme dapat berkembang dikarenakan kurangnya pemahaman akan cinta tanah air dan juga lemahnya pengetahuan terkait keagamaan, sehingga membuat pelaku mudah dicuci otaknya oleh paham radikalisme. Itulah alasan, betapa pentingnya edukasi mengenai Pancasila untuk dilakukan sejak dini dan bertahap bahkan harus diajarkan sampai jenjang perguruan tinggi, tapi tak hanya dilingkup pendidikan tapi juga harus diterapkan dalam kehidupan sehari-hari dan juga tindakan demi utuhnya NKRI dan juga mewujudkan SDM unggul yang cinta akan tanah air. Pemahaman Sila yang perlu ditekankan untuk menepis paham radikalisme adalah…",
         "jawaban": "Sila Pertama dan Sila Ketiga.",
         "pembahasan": "Paham radikalisme dapat ditepis dengan pemahaman ilmu keagamaan serta rasa cinta tanah air, hal ini merupakan nilai-nilai dalam Pancasila Sila Pertama dan Sila Ketiga.",
         "pilihan": [
           [
             "Sila Kedua dan Sila Ketiga.",
             0
           ],
           [
             "Sila Pertama dan Sila Keempat.",
             0
           ],
           [
             "Sila Pertama dan Sila Ketiga.",
             5
           ],
           [
             "Sila Pertama dan Sila Kedua.",
             0
           ],
           [
             "Sila Pertama dan Sila Kelima.",
             0
           ]
         ],
         "jawaban_skor": [
           "Sila Pertama dan Sila Ketiga.",
           5
         ]
       },
       {
         "id": 34,
         "soal": "Ciri positif demokrasi ekonomi berdasarkan Pancasila menurut UUD 1945 adalah...",
         "jawaban": "Cabang produksi penting dikuasai oleh negara.",
         "pembahasan": "Ciri demokrasi Pancasila adalah cabang produksi penting dikuasai oleh negara. Hal tersebut merupakan ciri positif, karena monopoli pihak swasta tidak akan dilakukan.",
         "pilihan": [
           [
             "Negara mengatur seluruh sistem ekonomi nasional.",
             0
           ],
           [
             "Perusahaan negara merupakan soko guru ekonomi.",
             0
           ],
           [
             "Setiap warga negara bebas bersaing dan berusaha.",
             0
           ],
           [
             "Cabang produksi penting dikuasai oleh negara.",
             5
           ],
           [
             "Pemerintah mengawasi perkembangan usaha swasta.",
             0
           ]
         ],
         "jawaban_skor": [
           "Cabang produksi penting dikuasai oleh negara.",
           5
         ]
       },
       {
         "id": 35,
         "soal": "Posisi Indonesia yang strategis menjadikan Indonesia sulit untuk menghindarkan diri dari pengaruh kebudayaan bangsa lain. Sejarah membuktikan, banyak negara asing memberi pengaruh budaya kita, tetapi dalam perjalanan sejarah kemudian, pengaruh budaya asing tersebut memunculkan kebiasaan dengan segala akibat negatifnya. Pemahaman Pancasila sangat berperan penting untuk mengatasi permasalahan dimaksud, hal ini diwujudkan dengan salah satu fungsi Pancasila dalam menyaring budaya asing adalah sebagai...",
         "jawaban": "Kepribadian bangsa.",
         "pembahasan": "Pancasila sebagai kepribadian bangsa berfungsi untuk menyaring budaya asing yang tidak sesuai dengan kepribadaian bangsa kita, supaya budaya asing yang tidak baik tidak masuk ke negara kita.",
         "pilihan": [
           [
             "Kepribadian bangsa.",
             5
           ],
           [
             "Perjanjian luhur bangsa.",
             0
           ],
           [
             "Filsafat hidup bangsa.",
             0
           ],
           [
             "Etika hidup bangsa.",
             0
           ],
           [
             "Dasar negara Indonesia.",
             0
           ]
         ],
         "jawaban_skor": [
           "Kepribadian bangsa.",
           5
         ]
       },
       {
         "id": 36,
         "soal": "Batas-batas keterbukaan Pancasila sebagai ideologi yang terbuka adalah, kecuali...",
         "jawaban": "Tidak perlu melalui konsensus dalam penciptaan norma baru.",
         "pembahasan": "Pancasila sebagai ideologi terbuka juga mempunyai batasan. Yang bukan merupakan batasannya adalah Pancasila tidak perlu melalui konsensus dalam penciptaan norma baru.",
         "pilihan": [
           [
             "Larangan terhadap ideologi Marxisme, Lenninisme, Komunisme.",
             0
           ],
           [
             "Menutup diri terhadap pandangan ekstrim yang meresaahkan masyarakat.",
             0
           ],
           [
             "Tidak perlu melalui konsensus dalam penciptaan norma baru.",
             5
           ],
           [
             "Mementingkan pada stabilitas nasional yang sehat dinamis.",
             0
           ],
           [
             "Mencegah berkembangnya paham dan ideologi liberal.",
             0
           ]
         ],
         "jawaban_skor": [
           "Tidak perlu melalui konsensus dalam penciptaan norma baru.",
           5
         ]
       },
       {
         "id": 37,
         "soal": "Kebijakan kenaikan iuran BPJS Kesehatan yang disetujui Presiden Joko Widodo (Jokowi) dan mulai berlaku pada 2020 menimbulkan pro kontra di tengah masyarakat. Para pengamat mengatakan bahwa jaminan kesehatan yang diberlakukan oleh BPJS harus tetap hidup terus, sehingga kenaikan iuran BPJS tersebut adalah hal yang rasional. Lantaran, pengadaan jaminan sosial dan kesehatan ini termasuk amanah dalam UUD 1945 yang diamandemen, khususnya...",
         "jawaban": "Pasal 28H dan Pasal 34 ayat (2) UUD 1945.",
         "pembahasan": "Pengaturan sistem dan hak memperoleh jaminan sosial diatur dalam Pasal 28H dan Pasal 34 ayat (2) UUD 1945.",
         "pilihan": [
           [
             "Pasal 28F dan Pasal 34 ayat (2) UUD 1945.",
             0
           ],
           [
             "Pasal 28H dan Pasal 34 ayat (2) UUD 1945.",
             5
           ],
           [
             "Pasal 28H dan Pasal 34 ayat (1) UUD 1945.",
             0
           ],
           [
             "Pasal 28I dan Pasal 34 ayat (2) UUD 1945.",
             0
           ],
           [
             "Pasal 28F dan Pasal 34 ayat (1) UUD 1945.",
             0
           ]
         ],
         "jawaban_skor": [
           "Pasal 28H dan Pasal 34 ayat (2) UUD 1945.",
           5
         ]
       },
       {
         "id": 38,
         "soal": "Pemerintah mengalokasikan dana sebesar Rp 132,2 triliun untuk anggaran kesehatan di tahun 2020. Angka ini naik hampir dua kali lipat dari realisasi anggaran kesehatan di tahun 2015 sebesar Rp 69,3 triliun. Pada tahun 2020, Pemerintah terus melanjutkan program prioritas di bidang kesehatan, dengan memperkuat layanan dan akses kesehatan di fasilitas kesehatan tingkat pertama, diikuti ketersediaan tenaga kesehatan yang berkualitas. Hal ini merupakan amanah dari...",
         "jawaban": "Pasal 34 ayat (3) UUD 1945.",
         "pembahasan": "Pasal 34 ayat (3) UUD 1945 mengatur bahwa Negara bertanggung jawab atas penyediaan fasilitas pelayanan kesehatan dan fasilitas pelayanan umum yang layak.",
         "pilihan": [
           [
             "Pasal 34 ayat (3) UUD 1945.",
             5
           ],
           [
             "Pasal 33 ayat (1) UUD 1945.",
             0
           ],
           [
             "Pasal 34 ayat (2) UUD 1945.",
             0
           ],
           [
             "Pasal 33 ayat (2) UUD 1945.",
             0
           ],
           [
             "Pasal 34 ayat (1) UUD 1945.",
             0
           ]
         ],
         "jawaban_skor": [
           "Pasal 34 ayat (3) UUD 1945.",
           5
         ]
       },
       {
         "id": 39,
         "soal": "Kewarganegaraan seseorang yang ditetapkan menurut pertalian darah oang tuanya adalah asas...",
         "jawaban": "Ius sanguinis.",
         "pembahasan": "Berdasarkan asas Ius Sanginis, seseorang menjadi warga negara suatu Negara dikarenakan adanya pertalian darah dengan orang tuanya.",
         "pilihan": [
           [
             "Staatenloos.",
             0
           ],
           [
             "Naturalisasi.",
             0
           ],
           [
             "Ius soli.",
             0
           ],
           [
             "Ius sanguinis.",
             5
           ],
           [
             "Kewarganegaraan rangkap.",
             0
           ]
         ],
         "jawaban_skor": [
           "Ius sanguinis.",
           5
         ]
       },
       {
         "id": 40,
         "soal": "Terdapat dua macam sumber hukum, yaitu sumber hukum formal dan sumber hukum material. Berikut ini yang termasuk sumber hukum material adalah...",
         "jawaban": "Jiwa bangsa",
         "pembahasan": "Yang termasuk sumber hukum material adalah nilai agama, kesusilaan, kehendak Tuhan, akal budi, jiwa bangsa, hubungan sosisal, kekuatan politik dan keadaan geografi.",
         "pilihan": [
           [
             "Jiwa bangsa",
             5
           ],
           [
             "Traktat",
             0
           ],
           [
             "Kebiasaan",
             0
           ],
           [
             "Yurisprudensi",
             0
           ],
           [
             "Undang-undang",
             0
           ]
         ],
         "jawaban_skor": [
           "Jiwa bangsa",
           5
         ]
       },
       {
         "id": 41,
         "soal": "Berikut ini yang merupakan kewenangan Mahkamah Agung adalah...",
         "jawaban": "Mengadili pada tingkat kasasi.",
         "pembahasan": "Wewenang Mahkamah Agung: \n 1) Memeriksa permohonan kasasi.\n 2) Menguji secara materiil hanya terhadap peraturan di bawah undang-undang.\n 3) Memberi nasihat hukum kepada Presiden selaku Kepala Negara dalam rangka pemberian dan penolakan grasi.\n 4) Memberi pertimbangan-pertimbangan di bidang hukum baik diminta maupun tidak kepada lembaga tinggi negara yang lain.",
         "pilihan": [
           [
             "Menguji UU terhadap UUD 1945.",
             0
           ],
           [
             "Mengadili perkara pidana di tingkat banding.",
             0
           ],
           [
             "Mengadili pada tingkat kasasi.",
             5
           ],
           [
             "Memutus sengketa hasil pemilu.",
             0
           ],
           [
             "Memberi putusan final pada persoalan yang berkaitan dengan persidangan agama.",
             0
           ]
         ],
         "jawaban_skor": [
           "Mengadili pada tingkat kasasi.",
           5
         ]
       },
       {
         "id": 42,
         "soal": "Salah satu hak MA adalah untuk menguji terhadap peraturan yang berlaku, kecuali...",
         "jawaban": "Undang – undang.",
         "pembahasan": "MA tidak berhak menguji Undang-Undang.",
         "pilihan": [
           [
             "Keputusan Menteri.",
             0
           ],
           [
             "Keputusan Presiden.",
             0
           ],
           [
             "Peraturan Daerah.",
             0
           ],
           [
             "Peraturan Pemerintah.",
             0
           ],
           [
             "Undang – undang.",
             5
           ]
         ],
         "jawaban_skor": [
           "Undang – undang.",
           5
         ]
       },
       {
         "id": 43,
         "soal": "Tugas utama Dewan Konstituante yang dibentuk berdasarkan hasil pemilu I tahun 1955 adalah...",
         "jawaban": "Membuat undang-undang dasar yang baru untuk menggantikan UUD 1950.",
         "pembahasan": "Dewan Konstituante yang dibentuk melalui pemilu I 1955 tidak dapat menjalankan tugasnya, yaitu membuat UUD yang baru untuk menggantikan UUD Sementara tahun 1950.",
         "pilihan": [
           [
             "Membuat dan menetapkan GBHN.",
             0
           ],
           [
             "Membuat rancangan RAPBN.",
             0
           ],
           [
             "Membuat undang-undang dasar yang baru untuk menggantikan UUD 1950.",
             5
           ],
           [
             "Membantu presiden dalam memilih menteri-menteri.",
             0
           ],
           [
             "Membuat undang-undang yang baru untuk menggatikan undang-undang RIS.",
             0
           ]
         ],
         "jawaban_skor": [
           "Membuat undang-undang dasar yang baru untuk menggantikan UUD 1950.",
           5
         ]
       },
       {
         "id": 44,
         "soal": "Di bawah ini yang bukan merupakan isi Dekrit Presiden 5 Juli 1959 adalah...",
         "jawaban": "Pembentukan DPRS.",
         "pembahasan": "Isi Dekrit Presiden 5 Juli 1959 adalah:\n 1) Pembubaran Dewan Konstituante\n 2) Kembali ke UUD 1945 dan tidak berlakunya UUDS\n 3) Pembentukan MPRS dan DPAS.",
         "pilihan": [
           [
             "Tidak berlakunya UUDS.",
             0
           ],
           [
             "Pembentukan DPRS.",
             5
           ],
           [
             "Pembubaran Dewan Konstituante.",
             0
           ],
           [
             "Kembali kepada UUD 1945.",
             0
           ],
           [
             "Pembentukan DPAS.",
             0
           ]
         ],
         "jawaban_skor": [
           "Pembentukan DPRS.",
           5
         ]
       },
       {
         "id": 45,
         "soal": "Jenderal sekutu yang tewas pada tanggal 30 Oktober 1945 dan memicu terjadinya peristiwa 10 November 1945 di Surabaya adalah ...",
         "jawaban": "Jenderal Mallaby.",
         "pembahasan": "Dalam pertempuran di Surabaya, Pemimpin Inggris yang bernama A.W.S. Mallaby tewas dalam pertempuran pada tanggal 25 Oktober 1945.",
         "pilihan": [
           [
             "Jenderal Stevenson.",
             0
           ],
           [
             "Jenderal Christison.",
             0
           ],
           [
             "Jenderal Christoper.",
             0
           ],
           [
             "Jenderal Mallaby.",
             5
           ],
           [
             "Jenderal Deandless.",
             0
           ]
         ],
         "jawaban_skor": [
           "Jenderal Mallaby.",
           5
         ]
       },
       {
         "id": 46,
         "soal": "Penulisan huruf miring yang benar adalah...",
         "jawaban": "Paman rajin minum Nigella Sativa untuk sakit reumatik yang dideritanya.",
         "pembahasan": "Penggunaan huruf miring yang tepat adalah:\n • penulisan nama buku, majalah, surat kabar;\n • untuk menegaskan atau mengkhususkan huruf, bagian kata, kata, atau kelompok kata; dan\n • menuliskan nama ilmiah atau ungkapan asing yang telah disesuaikan.\n Penggunaan yang benar adalah “Paman rajin munum Nigella sativa untuk reumatik yang dideritanya.”",
         "pilihan": [
           [
             "Paman rajin minum Nigella Sativa untuk sakit reumatik yang dideritanya.",
             5
           ],
           [
             "Liberalisme berbahaya bagi bangsa ini.",
             0
           ],
           [
             "Indonesia pernah mendapat julukan macan Asia.",
             0
           ],
           [
             "Halaman parkir di Dufan sangat luas.",
             0
           ],
           [
             "Sepatu ini diimpor dari Paris.",
             0
           ]
         ],
         "jawaban_skor": [
           "Paman rajin minum Nigella Sativa untuk sakit reumatik yang dideritanya.",
           5
         ]
       },
       {
         "id": 47,
         "soal": "Ibu mengepang rambutku dua-dua. Kata reduplikasi berikut yang bermakna sama dengan kata reduplikasi dalam kalimat tersebut adalah…",
         "jawaban": "Badut itu membagikan permen tangkai sebatang-sebatang tiap anak.",
         "pembahasan": "Makna kata reduplikasi dua-dua adalah menyatakan kelompok atau kumpulan yang berjumlah dua. Kalimat yang terdapat kata reduplikasi bermakna sama adalah “Badut itu membagikan permen tangkai sebatang-sebatang pada anak.”",
         "pilihan": [
           [
             "Konstruksi gedung di kawasan ini kuat-kuat.",
             0
           ],
           [
             "Pagi itu ia sudah marah-marah hanya karena sepatunya terinjak.",
             0
           ],
           [
             "Artis itu baru pulang syuting subuh-subuh.",
             0
           ],
           [
             "Badut itu membagikan permen tangkai sebatang-sebatang tiap anak.",
             5
           ],
           [
             "Bendahara diharapkan dapat membata ulang barang-barang di kantor ini.",
             0
           ]
         ],
         "jawaban_skor": [
           "Badut itu membagikan permen tangkai sebatang-sebatang tiap anak.",
           5
         ]
       },
       {
         "id": 48,
         "soal": "Monumen Tempat Lahir Jenderal Soedirman yang ... pada 21 Maret 1977 ini sudah masuk dalam kategori museum, ... pada International Council of Museum (ICOM). Karena itu, museum bersejarah tersebut harus dipelihara dan dilestarikan. Tidak hanya itu, ... adalah kebutuhan utama agar MTL Jenderal Soedirman lebih dikenal di luar Purbalingga. Kata-kata yang tepat untuk melengkapi paragraf tersebut adalah...",
         "jawaban": "diresmikan, merujuk, sosialisasi.",
         "pembahasan": "Kata yang tepat adalah diresmikan, merujuk, sosialisasi. Monumen Tempat Lahir Jenderal Soedirman yang diresmikan pada 21 Maret 1977 ini sudah masukdalam kategori museum merujuk pada International Council of Museum (ICOM). Karena itu, museum bersejarah tersebut harus dipeliharan dan dilestarikan. Tidak hanya itu, sosialisasi adalah kebutuhan utama agar MTL Jenderal Soedirman lebih dikenal di luar Purbalingga.",
         "pilihan": [
           [
             "dibuka, berdasarkan, pengenalan.",
             0
           ],
           [
             "diresmikan, berdasarkan, sosialisasi.",
             0
           ],
           [
             "diresmikan, merujuk, sosialisasi.",
             5
           ],
           [
             "dibebaskan, merujuk, sosialisasi.",
             0
           ],
           [
             "dilegalkan, menurut, pelestarian.",
             0
           ]
         ],
         "jawaban_skor": [
           "diresmikan, merujuk, sosialisasi.",
           5
         ]
       },
       {
         "id": 49,
         "soal": "Nilai-nilai dasar yang terkandung dalam sebuah ideologi secara riil hidup dalam dan bersumber dari budaya dan pengalaman sejarah masyarakat atau bangsanya adalah definisi ...",
         "jawaban": "Dimensi realitas.",
         "pembahasan": "Kekuatan dan kualitas sebuah ideologi ditentukan dari tiga dimensi, yaitu:\n 1) Dimensi Realitas adalah nilai-nilai dasar dari sebuah ideologi itu secara riil hidup di dalam masyarakat dan bersumber dari masyarakat.\n 2) Dimensi Idealisme adalah nilai-nilai dasar dari sebuah ideologi tersebut mengandung idealisme yang memberikan harapan masa depan yang lebih baik.\n 3) Dimensi Fleksibilitas adalah nilai-nilai dasar dari sebuah ideologi tersebut mempunyai keluwesan untuk dapat menyesuaikan diri dengan perkembangan masyarakat.",
         "pilihan": [
           [
             "Dimensi fleksibilitas.",
             0
           ],
           [
             "Dimensi instrumental.",
             0
           ],
           [
             "Dimensi nilai dasar.",
             0
           ],
           [
             "Dimensi idealisme.",
             0
           ],
           [
             "Dimensi realitas.",
             5
           ]
         ],
         "jawaban_skor": [
           "Dimensi realitas.",
           5
         ]
       },
       {
         "id": 50,
         "soal": "Bangsa Indonesia mendambakan pelaksanaan pembangunan berdasarkan paradigma Pancasila. Dengan demikian, pembangunan yang telah dilaksanakan hendaknya ...",
         "jawaban": "Untuk mewujudkan manusia dan masyarakat Indonesia seutuhnya.",
         "pembahasan": "Bangsa Indonesia mendambakan pelaksanaan pembangunan berdasarkan paradigma Pancasila. Dengan demikian, pembangunan yang telah dilaksanakan hendaknya untuk mewujudkan manusia dan masyarakat Indonesia seutuhnya.",
         "pilihan": [
           [
             "Untuk mewujudkan suatu masyarakat madani yang mempunyai kepribadian maju.",
             0
           ],
           [
             "Untuk menghasilkan produk-produk yang kompetitif pada pasar internasional.",
             0
           ],
           [
             "Menghasilkan manusia dan masyarakat yang maju dengan kepribadian Indonesia.",
             0
           ],
           [
             "Demi kelangsungan generasi penerus yang bertanggung jawab.",
             0
           ],
           [
             "Untuk mewujudkan manusia dan masyarakat Indonesia seutuhnya.",
             5
           ]
         ],
         "jawaban_skor": [
           "Untuk mewujudkan manusia dan masyarakat Indonesia seutuhnya.",
           5
         ]
       },
       {
         "id": 51,
         "soal": "Nilai-nilai pada sila keempat meliputi dan menjiwai sila ke- ...",
         "jawaban": "5",
         "pembahasan": "v Sila pertama meliputi dan menjiwai sila ke-II, III, IV, dan V. \n v Sila kedua meliputi dan menjiwai sila ke-III, IV, dan V. \n v Sila ketiga meliputi dan menjiwai sila ke-IV, dan V. \n v Sila keempat meliputi dan menjiwai sila ke-V.",
         "pilihan": [
           [
             "2",
             0
           ],
           [
             "3",
             0
           ],
           [
             "4",
             0
           ],
           [
             "1",
             0
           ],
           [
             "5",
             5
           ]
         ],
         "jawaban_skor": [
           "5",
           5
         ]
       },
       {
         "id": 52,
         "soal": "Kemanusiaan yang adil dan beradab merupakan salah satu perwujudan dari sikap kebhinnekaan di Indonesia artinya kebhinnekaan itu dapat mewujudkan ...",
         "jawaban": "Hubungan sosial yang selaras, serasi, dan seimbang.",
         "pembahasan": "Kemanusiaan yang adil dan beradab merupakan salah satu perwujudan dari sikap kebhinnekaan di Indonesia artinya kebhinnekaan itu dapat mewujudkan hubungan sosial yang selaras, serasi, dan seimbang.",
         "pilihan": [
           [
             "Kebebasan dalam memberikan pendapat.",
             0
           ],
           [
             "Keamanan dan kemampuan menjawab tantangan zaman.",
             0
           ],
           [
             "Sikap saling menghormati dalam bekerja sama.",
             0
           ],
           [
             "Keyakinan akan persatuan dan kesatuan.",
             0
           ],
           [
             "Hubungan sosial yang selaras, serasi, dan seimbang.",
             5
           ]
         ],
         "jawaban_skor": [
           "Hubungan sosial yang selaras, serasi, dan seimbang.",
           5
         ]
       },
       {
         "id": 53,
         "soal": "Tim kecil tujuh orang yang khusus merumuskan rancangan UUD diketuai oleh ...",
         "jawaban": "Ir. Soekarno.",
         "pembahasan": "Pada tanggal 11 Juli 1945 (masa sidang BPUPKI yang kedua) dibentuklah Panitia Perancang UUD yang diketuai oleh Ir. Soekarno dan beranggotakan tujuh orang (Prof. Dr. Supomo, Wongsonegoro, Achmad Subardjo, A.A. Maramis, R.P. Singgih, H.A.M. Agus Salim, dan Dr. Sukiman). Tugas panitia ini membuat rancangan undang-undang dasar. Hasil kerja panitia itu dilaporkan kepada Panitia Perancang Undang-Undang Dasar dan diterima pada tanggal 13 Juli 1945. Pada persidangan tanggal 14 Juli 1945, Ir. Sukarno melaporkan hasil kerja seluruh panitia yang mencakup tiga hal, yaitu: Pernyataan Indonesia merdeka,Pembukaan undang-undang dasar,Undang-undang dasar itu sendiri (batang tubuh).",
         "pilihan": [
           [
             "Achmad Soebardjo.",
             0
           ],
           [
             "Moh. Hatta.",
             0
           ],
           [
             "KPRT Wongsonegoro.",
             0
           ],
           [
             "Soepomo.",
             0
           ],
           [
             "Ir. Soekarno.",
             5
           ]
         ],
         "jawaban_skor": [
           "Ir. Soekarno.",
           5
         ]
       },
       {
         "id": 54,
         "soal": "Sistem konstitusi RIS mulai dilaksanakan secara resmi pada tanggal ...",
         "jawaban": "27 Desember 1949.",
         "pembahasan": "Konstitusi RIS resmi dijalankan di Indonesia mulai tanggal 27 Desember 1949. Pada saat itu konstitusi yang berlaku adalah UU RIS dengan kepala negara presiden Soekarno dan kepala pemerintahannya adalah perdana menteri Moh. Hatta.",
         "pilihan": [
           [
             "17 Agustus 1950.",
             0
           ],
           [
             "2 November 1949.",
             0
           ],
           [
             "30 Desember 1949.",
             0
           ],
           [
             "5 Juli 1949.",
             0
           ],
           [
             "27 Desember 1949.",
             5
           ]
         ],
         "jawaban_skor": [
           "27 Desember 1949.",
           5
         ]
       },
       {
         "id": 55,
         "soal": "Dalam rangka membina rasa nasionalisme di kalangan masyarakat Indonesia hendaknya dilakukan dengan menghindari hal-hal di bawah ini, kecuali ...",
         "jawaban": "Patriotisme.",
         "pembahasan": "Patriotisme adalah sikap seseorang yang bersedia mengorbankan segala-galanya untuk kejayaan dan kemakmuran tanah airnya, hal ini jusru mendukung nasionalismedi Indonesia.",
         "pilihan": [
           [
             "Jingoisme.",
             0
           ],
           [
             "Ekstrimisme.",
             0
           ],
           [
             "Sukuisme.",
             0
           ],
           [
             "Chauvinisme.",
             0
           ],
           [
             "Patriotisme.",
             5
           ]
         ],
         "jawaban_skor": [
           "Patriotisme.",
           5
         ]
       },
       {
         "id": 56,
         "soal": "Peranan UUD 1945 dalam sistem ketatanegaraan RI adalah sebagai ...",
         "jawaban": "Hukum Dasar.",
         "pembahasan": "UUD 1945 merupakan konstitusi atau hukum dasar yang dilaksanakan di Indonesia. .",
         "pilihan": [
           [
             "Hukum Dasar.",
             5
           ],
           [
             "Konstitusi Sementara.",
             0
           ],
           [
             "Hukum Internasional.",
             0
           ],
           [
             "Hukum Tidak Tertulis.",
             0
           ],
           [
             "Hukum Adat.",
             0
           ]
         ],
         "jawaban_skor": [
           "Hukum Dasar.",
           5
         ]
       },
       {
         "id": 57,
         "soal": "Pembahasan rancangan UUD 1945 dilakukan oleh suatu badan resmi yang bernama ...",
         "jawaban": "BPUPKI.",
         "pembahasan": "Pada tanggal 11 Juli 1945 (masa sidang BPUPKI yang kedua) dibentuklah Panitia Perancang UUD yang diketuai oleh Ir. Soekarno dan beranggotakan tujuh orang. Panitia Perancang UUD inilah yang menjadi panitia kecil dalam BPUPKI yang bertugas untuk menyusun dan membahas Rancangan Undang-Undang Dasar.",
         "pilihan": [
           [
             "PPKI.",
             0
           ],
           [
             "BPUPKI.",
             5
           ],
           [
             "Dokuritsu Zyunbi Inkai.",
             0
           ],
           [
             "Dewan Konstituante.",
             0
           ],
           [
             "KNIP.",
             0
           ]
         ],
         "jawaban_skor": [
           "BPUPKI.",
           5
         ]
       },
       {
         "id": 58,
         "soal": "Suatu perilaku konstitusional dalam kehidupan berbangsa dan bernegara antara lain adalah ...",
         "jawaban": "Taat dan tertib pada lalu lintas.",
         "pembahasan": "Suatu perilaku konstitusional dalam kehidupan berbangsa dan bernegara antara lain adalah taat dan tertib pada lalu lintas.",
         "pilihan": [
           [
             "Menghormati Sang Merah Putih pada saat upacara Bendera.",
             0
           ],
           [
             "Taat dan tertib pada lalu lintas.",
             5
           ],
           [
             "Patuh terhadap perintah saat ada yang melihat.",
             0
           ],
           [
             "Sabar dalam menerima segala kepahitan dan kejamnya hidup.",
             0
           ],
           [
             "Ikut bergotong royong karena ada kepentingan.",
             0
           ]
         ],
         "jawaban_skor": [
           "Taat dan tertib pada lalu lintas.",
           5
         ]
       },
       {
         "id": 59,
         "soal": "Dalam memberhentikan Presiden dan/ atau Wakil Presiden, MPR akan memutuskannya berdasarkan putusan dari ...",
         "jawaban": "MK.",
         "pembahasan": "Dalam Pasal 7B UUD 1945 diatur bahwa dalam memberhentikan Presiden dan/ atau Wakil Presiden, MPR akan memutuskannya berdasarkan putusan dari MK.",
         "pilihan": [
           [
             "KY.",
             0
           ],
           [
             "MK.",
             5
           ],
           [
             "DPR.",
             0
           ],
           [
             "MA.",
             0
           ],
           [
             "DPD.",
             0
           ]
         ],
         "jawaban_skor": [
           "MK.",
           5
         ]
       },
       {
         "id": 60,
         "soal": "Jika terjadi kekosongan Wakil Presiden dalam masa jabatannya, yang berhak mengusulkan daftar calon wakil presiden adalah ...",
         "jawaban": "Presiden.",
         "pembahasan": "Dalam Pasal 8 ayat (2) UUD 1945 diatur bahwa dalam hal terjadi kekosongan Wakil Presiden, selambatlambatnya dalam waktu enam puluh hari, Majelis Permusyawaratan Rakyat menyelenggarakan sidang untuk memilih Wakil Presiden dari dua calon yang diusulkan oleh Presiden.",
         "pilihan": [
           [
             "DPR.",
             0
           ],
           [
             "DPR dengan pertimbangan Presiden.",
             0
           ],
           [
             "Partai pemenang pemilu.",
             0
           ],
           [
             "Presiden dengan pertimbangan DPR.",
             0
           ],
           [
             "Presiden.",
             5
           ]
         ],
         "jawaban_skor": [
           "Presiden.",
           5
         ]
       }
     ]
   }
 }