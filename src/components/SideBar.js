import React from 'react'
import user from '../images/header/user.png'
import home from '../images/sideBar/home.svg'
import assign from '../images/sideBar/assign.svg'
import lock from '../images/sideBar/lock.svg'
import setting from '../images/sideBar/setting.svg'
import logout from '../images/sideBar/logout.svg'
import '../styles/sideBar.css'

export const SideBar = () => {
    return (
        <div id="sideBar" className="bg-white">
            <img src={user} alt="user profile" />
            <p className="text-center dark-text bold-text mt-2">Pengguna 001</p>
            <div className="px-4 mt-5 d-flex align-items-center">
                <img src={home} alt="home" className="mr-2" />
                <p className="bold-text dark-text">Home</p>
            </div>
            <p className="mt-4 bold-text dark-text px-4 pb-2" style={{ opacity: 0.7, borderBottom: "1px solid #E0E0E0" }}>Simulasi Tryout</p>
            <div className="px-4 mt-3 d-flex align-items-center">
                <img src={assign} alt="Tryout Free" className="mr-2" />
                <p className="bold-text dark-text">Tryout Free</p>
            </div>
            <div className="px-4 mt-3 d-flex align-items-center">
                <img src={lock} alt="Tryout Nasional" className="mr-2" />
                <p className="bold-text dark-text">Tryout Nasional</p>
            </div>
            <div className="px-4 mt-3 d-flex align-items-center pb-3" style={{borderBottom: "1px solid #E0E0E0"}}>
                <img src={lock} alt="Tryout Premium" className="mr-2" />
                <p className="bold-text dark-text">Tryout Premium</p>
            </div>
            <div className="px-4 mt-3 d-flex align-items-center">
                <img src={setting} alt="Setting" className="mr-2" />
                <p className="bold-text dark-text">Pengaturan</p>
            </div>
            <div className="px-4 mt-5 d-flex align-items-center sidebar-logout">
                <img src={logout} alt="Logout" className="mr-2" />
                <p className="bold-text dark-text">Logout</p>
            </div>
        </div>
    )
}
