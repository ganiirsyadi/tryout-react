import React, { useState, useContext, useEffect } from 'react'
import { TryoutContext } from '../../contexts/TryoutState'
import flag from '../../images/tryout/flag.svg'

export const Soal = () => {

    const { state, changeNumber, saveChoice, markNumber } = useContext(TryoutContext)
    const currentState = state[`${state.lastType}`]

    const [daftarPilihan, setDaftarPilihan] = useState(currentState.soal[parseInt(currentState.lastNumber) - 1].pilihan)

    const [pilihan, setPilihan] = useState(currentState.answer[currentState.lastNumber])

    function choiceHandler(e) {
        setPilihan(e.target.value)
        saveChoice(e.target.value)
    }

    useEffect(() => {
        setDaftarPilihan(currentState.soal[currentState.lastNumber - 1].pilihan)
        setPilihan(currentState.answer[currentState.lastNumber - 1])        
    }, [currentState.answer, currentState.lastNumber, currentState.soal, state.lastType])

    return (
        <div id="soal">
            <div className="bg-white soal-card">
                <div className="d-flex justify-content-between align-items-center mb-4">
                    <h5 className="bold-text">Soal {currentState.lastNumber}</h5>
                    <div className="d-flex" onClick={() => markNumber(currentState.lastNumber)}>
                        <img src={flag} alt="tandai" className="mr-2" />
                        <h5
                            className="bold-text m-0"
                            style={{ color: "#EB5757", cursor: "pointer" }}
                        >Tandai</h5>
                    </div>
                </div>
                <p>{currentState.soal[parseInt(currentState.lastNumber) - 1].soal}</p>
                <form action="" className="mt-3 ml-3">
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="pilihan" id="radio-1"
                            value={daftarPilihan[0][0]} checked={pilihan === daftarPilihan[0][0]} onChange={choiceHandler} />
                        <label className="form-check-label" htmlFor="radio-1">
                            {daftarPilihan[0][0]}
                        </label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="pilihan" id="radio-2"
                            value={daftarPilihan[1][0]} checked={pilihan === daftarPilihan[1][0]} onChange={choiceHandler} />
                        <label className="form-check-label" htmlFor="radio-2">
                            {daftarPilihan[1][0]}
                        </label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="pilihan" id="radio-3"
                            value={daftarPilihan[2][0]} checked={pilihan === daftarPilihan[2][0]} onChange={choiceHandler} />
                        <label className="form-check-label" htmlFor="radio-3">
                            {daftarPilihan[2][0]}
                        </label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="pilihan" id="radio-4"
                            value={daftarPilihan[3][0]} checked={pilihan === daftarPilihan[3][0]} onChange={choiceHandler} />
                        <label className="form-check-label" htmlFor="radio-4">
                            {daftarPilihan[3][0]}
                        </label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="pilihan" id="radio-5"
                            value={daftarPilihan[4][0]} checked={pilihan === daftarPilihan[4][0]} onChange={choiceHandler} />
                        <label className="form-check-label" htmlFor="radio-5">
                            {daftarPilihan[4][0]}
                        </label>
                    </div>
                </form>
            </div>
            <div className="soal-bottom my-3 d-flex justify-content-center">
                <button
                    className={`btn mr-2 btn-prev bold-text ${currentState.lastNumber === 1 && "d-none"}`}
                    onClick={() => changeNumber(currentState.lastNumber - 1)}
                >Sebelumnya</button>
                <button
                    className={`btn ml-2 btn-next bold-text ${currentState.lastNumber === currentState.soal.length && "d-none"}`}
                    onClick={() => changeNumber(currentState.lastNumber + 1)}
                >Selanjutnya</button>
            </div>
        </div>
    )
}
