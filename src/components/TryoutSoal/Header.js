import React, { useContext, useState, useEffect } from 'react'
import { TryoutContext } from '../../contexts/TryoutState'

export const Header = () => {

    const { namaPaket, timeEnd } = useContext(TryoutContext).state
    const [timeLeft, setTimeLeft] = useState(timeEnd)

    useEffect(() => {
        const timeLeftInterval = setInterval(() => {
            setTimeLeft(prevState => prevState - 1)
        }, 1000)

        return () => clearInterval(timeLeftInterval)
    }, [timeLeft])

    // Find the distance between now and the count down date
    let now = new Date().getTime();
    let distance = timeLeft - now;

    // Time calculations for days, hours, minutes and seconds
    let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));

    return (
        <header className="d-flex justify-content-between align-items-end flex-wrap">
            <h4 className="bold-text">{namaPaket}</h4>
            <h5 className="bold-text" id="time-left">Sisa Waktu: {hours} jam {minutes} menit</h5>
        </header>
    )
}
