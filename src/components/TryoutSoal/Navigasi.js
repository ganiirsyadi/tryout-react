import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import { TryoutContext } from '../../contexts/TryoutState'

export const Navigasi = () => {

    const { state, changeNumber } = useContext(TryoutContext)
    const currentState = state[`${state.lastType}`]

    const generateTile = (total) => {
        let result = []
        for (let i = 1; i <= total; i++) {
            result.push(
                <div
                    key={i}
                    className={`bold-text nav-box 
                        ${i === currentState.lastNumber && "nav-box-active"}
                        ${currentState.arrMarked.includes(i) && 'nav-box-marked'}
                    `}
                    onClick={() => changeNumber(i)}>{i}</div>
            )
        }
        return result
    }

    return (
        <div id="navigasi" className="bg-white align-self-start">
            <h5 className="bold-text">Navigasi Soal TIU</h5>
            <div className="grid mt-5">
                {state.lastType === "twk" ? generateTile(30) : generateTile(35)}
            </div>
            <Link className="normal-link" to="/hasil" >
                <p className="bold-text mt-3 text-right" style={{ color: "#EB5757", cursor: "pointer"}}>Finish Attempt</p>
            </Link>
        </div>
    )
}
