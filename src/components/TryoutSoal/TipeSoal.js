import React, { useContext } from 'react'
import { TryoutContext } from '../../contexts/TryoutState'

export const TipeSoal = () => {

    const { lastType } = useContext(TryoutContext).state
    const { changeType } = useContext(TryoutContext)

    return (
        <nav id="tipe-soal" className="d-flex">
            <h6 className={`bold-text ${lastType === "twk" && "nav-soal-active"}`} onClick={() => changeType("twk")}>TWK</h6>
            <h6 className={`bold-text ${lastType === "tiu" && "nav-soal-active"}`} onClick={() => changeType("tiu")}>TIU</h6>
            <h6 className={`bold-text ${lastType === "tkp" && "nav-soal-active"}`} onClick={() => changeType("tkp")}>TKP</h6>
        </nav>
    )
}
