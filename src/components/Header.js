import React from 'react'
import { Link } from 'react-router-dom'
import burgerMenu from '../images/header/burgerMenu.svg'
import logo from '../images/header/logo.png'
import notif from '../images/header/notification.svg'
import user from '../images/header/user.png'
import '../styles/header.css'
import { SideBar } from './SideBar'

export const Header = () => {

    const toggleSideBar = () => {
        document.getElementById("sideBar").classList.toggle("open-sideBar")
    }

    return (
        <>
            <nav id="navbar" className="bg-white d-flex">
                <img src={burgerMenu} alt="burgerMenu" onClick={toggleSideBar} />
                <Link className="m-auto" to="/">
                    <img src={logo} height="30px" alt="logo supertentor" />
                </Link>
                <div className="right-header d-flex align-items-center">
                    <img src={notif} alt="" />
                    <p className="dark-text bold-text mx-3">Pengguna 001</p>
                    <img src={user} alt="user profile" />
                </div>
            </nav>
            <SideBar />
        </>
    )
}
