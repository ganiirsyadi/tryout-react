import React from 'react'
import { Header } from '../components/TryoutSoal/Header'
import "../styles/containers/tryoutSoal.css"
import { Soal } from '../components/TryoutSoal/Soal'
import { Navigasi } from '../components/TryoutSoal/Navigasi'
import { TipeSoal } from '../components/TryoutSoal/TipeSoal'

export const TryoutSoal = () => {
    return (
            <div id="tryout-soal">
                <Header />
                <TipeSoal />
                <div className="d-flex flex-wrap justify-content-between">
                    <Soal />
                    <Navigasi />
                </div>
            </div>
    )
}
