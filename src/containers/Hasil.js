import React, { useContext } from 'react'
import "../styles/containers/hasil.css"
import { TryoutContext } from '../contexts/TryoutState'
import { Link } from 'react-router-dom'

export const Hasil = () => {

    const { state, deleteState } = useContext(TryoutContext)
    console.log(state);
    const hitungSkor = type => {
        let score = 0
        type.soal.forEach((val, i) => {
            let jawaban = type.answer[i]
            score += jawaban === val.jawaban_skor[0] ? val.jawaban_skor[1] : 0
        })
        return score
    }

    return (
        <div id="hasil">
            <h2 className="bold-text text-center">Hasil Tryout</h2>
            <div className="d-flex flex-column flex-md-row justify-content-between mt-3 mt-md-5 p-3">
                <div className="d-flex flex-column align-items-center">
                    <h4 className="score-type mt-3 mt-md-auto">TWK</h4>
                    <h4 className="bold-text score-right mt-md-3">Score : {hitungSkor(state.twk)}</h4>
                </div>
                <div className="d-flex flex-column align-items-center">
                    <h4 className="score-type mt-3 mt-md-auto">TIU</h4>
                    <h4 className="bold-text score-right mt-md-3">Score : {hitungSkor(state.tiu)}</h4>
                </div>
                <div className="d-flex flex-column align-items-center">
                    <h4 className="score-type mt-3 mt-md-auto">TKP</h4>
                    <h4 className="bold-text score-right mt-md-3">Score : {hitungSkor(state.tkp)}</h4>
                </div>
            </div>
            <Link to='/tryout' className="btn bold-text mt-5 btn-secondary d-flex justify-content-center mx-5" onClick={deleteState} >
                Ulangi Ujian
            </Link>
        </div>
    )
}
