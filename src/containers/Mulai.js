import React from 'react'
import { Link } from 'react-router-dom'

export const Mulai = () => {

    return (
        <div className="d-flex flex-column justify-content-center align-items-center" style={{ height: "calc(100vh - 60px)" }}>
            <Link
                className="btn"
                style={{ backgroundColor: "#67A69D" }}
                to="/tryout"
            >
                <h3 className="bold-text text-white m-0 px-3 py-2">Mulai Tryout</h3>
            </Link>
            <div style={{ position: "absolute", bottom: "30px" }}>
                <a href="http://supertentor.com">
                    <h3 className="mt-3 text-center">Supertentor.com</h3>
                </a>
                <p className="text-center">Kunjungi situs kami untuk tryout yang lebih lengkap</p>
            </div>
        </div>
    )
}
