import React, { createContext, useReducer } from 'react'
import { data } from '../tryoutData'
import TryoutReducer from './TryoutReducer'

// initial state

let temp1 = []
let temp2 = []
for (let i = 1; i <= 35; i++) {
    temp1.push("")
    temp2.push(0)
}

// deep copy the default state
let initialState = {
    namaPaket: data.paket.nama,
    lastType: "twk",
    tkp: {
        lastNumber: 1,
        arrMarked: [],
        answer: [...temp1],
        soal: data.paket.soal_tkp
    },
    twk: {
        lastNumber: 1,
        arrMarked: [],
        answer: temp1.slice(0, 30),
        soal: data.paket.soal_twk
    },
    tiu: {
        lastNumber: 1,
        arrMarked: [],
        answer: [...temp1],
        soal: data.paket.soal_tiu
    },
    timeEnd: parseInt(Date.now() + (90 * 60000))
}

if (sessionStorage.getItem("tryoutState") !== null) {
    initialState = JSON.parse(sessionStorage.getItem("tryoutState"))
}

// Create Context
export const TryoutContext = createContext(initialState)

// Provider Component

// 1. Provider component akan me-wrap component2 di App.js yang akan menggunakan context ini
// 2. children di situ maksudnya adalah component yang diwrap
// 3. value berisi object apa yang akan dishare
// cek TryoutReducer.js

export const TryoutProvider = ({ children }) => {
    const [state, dispatch] = useReducer(TryoutReducer, initialState)
        
    // action
    // cek TryoutReducer
    function changeNumber(number) {
        dispatch({
            type: "CHANGE_NUMBER",
            payload: number
        })
    }

    function changeType(type) {
        dispatch({
            type: "CHANGE_TYPE",
            payload: type
        })
    }

    function saveChoice(choiceVal) {
        dispatch({
            type: "SAVE_CHOICE",
            payload: choiceVal
        })
    }

    function markNumber(number) {
        dispatch({
            type: "MARK_NUMBER",
            payload: number
        })
    }

    function deleteState() {
        dispatch({
            type: "DELETE_STATE",
        })
    }

    return (
        <TryoutContext.Provider
            value={{
                state,
                changeNumber,
                changeType,
                saveChoice,
                deleteState,
                markNumber
            }}
        >
            {children}
        </TryoutContext.Provider>
    )
}