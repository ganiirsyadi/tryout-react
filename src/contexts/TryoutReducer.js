// reducer adalah bagaimana cara menentukan perubahan state terhadap suatu action terhadap context

// intinya dia nerima type sama payload dari GlobalState trus pake switch buat nentuin type apa, secara default berarti state ga berubah
// tiap case dia return state baru hasil proses
import { data } from '../tryoutData'

export default (state, action) => {

    let tempState = state

    switch (action.type) {

        case "CHANGE_NUMBER":
            tempState[`${state.lastType}`].lastNumber = action.payload
            sessionStorage.setItem("tryoutState", JSON.stringify(tempState))
            return {
                ...tempState,
            }

        case "CHANGE_TYPE":
            tempState.lastType = action.payload
            sessionStorage.setItem("tryoutState", JSON.stringify(tempState))
            return {
                ...tempState
            }

        case "SAVE_CHOICE":
            const lastNumber = tempState[`${tempState.lastType}`].lastNumber - 1
            tempState[`${tempState.lastType}`].answer[lastNumber] = action.payload
            sessionStorage.setItem("tryoutState", JSON.stringify(tempState))
            return {
                ...tempState,
            }

        case "MARK_NUMBER":
            tempState[`${tempState.lastType}`].arrMarked.push(action.payload)
            sessionStorage.setItem("tryoutState", JSON.stringify(tempState))
            return {
                ...tempState,
            }

        case "DELETE_STATE":

            sessionStorage.removeItem("tryoutState")

            let temp1 = []
            let temp2 = []
            for (let i = 1; i <= 35; i++) {
                temp1.push("")
                temp2.push(0)
            }

            return {
                namaPaket: data.paket.nama,
                lastType: "twk",
                tkp: {
                    lastNumber: 1,
                    arrMarked: [],
                    answer: [...temp1],
                    soal: data.paket.soal_tkp
                },
                twk: {
                    lastNumber: 1,
                    arrMarked: [],
                    answer: temp1.slice(0, 30),
                    soal: data.paket.soal_twk
                },
                tiu: {
                    lastNumber: 1,
                    arrMarked: [],
                    answer: [...temp1],
                    soal: data.paket.soal_tiu
                },
                timeEnd: parseInt(Date.now() + (90 * 60000))
            }

        default:
            return state
    }
}